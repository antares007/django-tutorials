# DjangoTutorials

## 介绍
Django项目培训

## 软件架构
软件架构说明

后端框架使用 ：Django  

前端框架使用：JQuery , Bootstrap


## 安装教程

项目使用 miniconda 来管理Python的虚拟环境。

对每一个子项目都是用一个独立的虚拟环境

### 一、安装 MiniConda 

很简单，不写了。

#### 1、 conda 命令的使用

会创建虚拟环境，会安装python包就可以了

### 二、 创建Django项目的虚拟环境

#### 1、 创建虚拟环境

首先，我们创建一个名称为 `djg329env` 的这样一个python虚拟环境, 命令如下所示：

```bash
conda create --name djg329env python==3.9.1
```

#### 2、 安装 django

可以使用 `conda install django` 来安装，不过安装的包可能不是最新发布的；

也可以使用 `pip install django` 来安装，这样安装的包始终是最新发布的。

安装完之后，是这样的：

```bash
(djg329env) F:\WebProjects>pip list
Package      Version
------------ ---------
asgiref      3.4.1
certifi      2021.10.8
Django       3.2.9
pip          21.2.4
pytz         2021.3
setuptools   58.0.4
sqlparse     0.4.2
wheel        0.37.0
wincertstore 0.2
```

## 建立项目文件夹

为了记录每一步开发过程，我们在码云(gitee.com)上创建一个代码仓库，来记录和管理项目的开发进度。

假设 我们已经创建好了仓库，名称为： ``DjangoTutorials`` ; 地址为 ： ``https://gitee.com/antares007/django-tutorials`` 。

我们现在就可以把这个仓库拉取到本地的某个文件夹中，作为本地代码仓库，命令如下：

```bash
Microsoft Windows [版本 10.0.19043.1348]
(c) Microsoft Corporation。保留所有权利。

F:\WebProjects>git clone https://gitee.com/antares007/django-tutorials
Cloning into 'django-tutorials'...
remote: Enumerating objects: 6, done.
remote: Total 6 (delta 0), reused 0 (delta 0), pack-reused 6
Receiving objects: 100% (6/6), done.

F:\WebProjects>

```

这样我们就在本地电脑的 ``F:\WebProjects`` 目录下创建了一个代码仓库： ``django-tutorials``

目录结构如下所示：

```bash
django-tutorials\
	.git\
	.gitignore
	READEME.md
```