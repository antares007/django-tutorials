"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.views import LoginView
from django.urls import path, re_path, include
from django.views.static import serve

urlpatterns = [
    # 后台管理系统的URLs
    path('admin/', admin.site.urls),
    # 用户登录视图
    path('accounts/login/', LoginView.as_view()),
    # 投票App的URLs
    path('polls/', include('polls.urls', namespace='polls')),

]

# DEBUG = true 模式下的路由配置
if settings.DEBUG:
    urlpatterns += [
        # 用于访问本地媒体文件的URL
        re_path(r'media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
    ]