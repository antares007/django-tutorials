# 创建Django初始项目并运行起来

## 一、检查环境

我们假定你已经安装了 Django，且安装的是哪个版本，通过在命令提示行输入命令（由 $ 前缀）。

如下所示：

```python
Microsoft Windows [版本 10.0.19043.1348]
(c) Microsoft Corporation。保留所有权利。

F:\WebProjects>conda info -e
# conda environments:
#
base                  *  D:\Programs\Miniconda3
aisitenv                 D:\Programs\Miniconda3\envs\aisitenv
cplt3d                   D:\Programs\Miniconda3\envs\cplt3d
djg329env                D:\Programs\Miniconda3\envs\djg329env
pth19                    D:\Programs\Miniconda3\envs\pth19
sdxenv                   D:\Programs\Miniconda3\envs\sdxenv
sklenv                   D:\Programs\Miniconda3\envs\sklenv


F:\WebProjects>conda activate djg329env

(djg329env) F:\WebProjects>python -m django --version
3.2.9

(djg329env) F:\WebProjects>

```

如果这行命令输出了一个版本号，证明你已经安装了此版本的 Django；如果你得到的是一个“No module named django”的错误提示，则表明你还未安装。

这个教程是为了 Django 3.2 写的，它支持 Python 3.6 和后续版本。如果 Django 的版本不匹配，你可以通过页面右下角的版本切换器切换到对应你版本的教程，或更新至最新版本。
如果你正在使用一个较老版本的 Python，在 我应该使用哪个版本的 Python 来配合 Django? 查找一个合适的 Django 版本。

![python版本与django版本的支持关系](./images/django-python-versions.png)

## 二、创建初始项目

如果这是你第一次使用 Django 的话，你需要一些初始化设置。也就是说，你需要用一些自动生成的代码配置一个 Django project —— 即一个 Django 项目实例需要的设置项集合，包括数据库配置、Django 配置和应用程序配置等。

### 1、使用 django-admin 创建项目

DJango 为我们提供了一个创建初始工程模板的命令行工具： ``django-admin`` 。 打开命令行，激活之前安装的虚拟环境 (djg329env)，可以查看该命令行的可用命令，如下所示：


```bash
F:\WebProjects>conda activate djg329env

(djg329env) F:\WebProjects>django-admin help

Type 'django-admin help <subcommand>' for help on a specific subcommand.

Available subcommands:

[django]
    check
    compilemessages
    createcachetable
    dbshell
    diffsettings
    dumpdata
    flush
    inspectdb
    loaddata
    makemessages
    makemigrations
    migrate
    runserver
    sendtestemail
    shell
    showmigrations
    sqlflush
    sqlmigrate
    sqlsequencereset
    squashmigrations
    startapp
    startproject
    test
    testserver
Note that only Django core commands are listed as settings are not properly configured (error: Requested setting INSTALLED_APPS, but settings are not configured. You must either define the environment variable DJANGO_SETTINGS_MODULE or call settings.configure() before accessing settings.).

(djg329env) F:\WebProjects>
```

我们可以看到 上面列举出了 ``django-admin`` 的所有可用命令，其中有一个 是 `startproject` ， 我们可以用下面的方式查看该命令的使用方法，如下所示：

```bash
(djg329env) F:\WebProjects>django-admin help startproject

usage: django-admin startproject [-h] [--template TEMPLATE] [--extension EXTENSIONS] [--name FILES] [--version]
                                 [-v {0,1,2,3}] [--settings SETTINGS] [--pythonpath PYTHONPATH] [--traceback]
                                 [--no-color] [--force-color]
                                 name [directory]

Creates a Django project directory structure for the given project name in the current directory or optionally in the
given directory.

positional arguments:
  name                  Name of the application or project.
  directory             Optional destination directory

optional arguments:
  -h, --help            show this help message and exit
  --template TEMPLATE   The path or URL to load the template from.
  --extension EXTENSIONS, -e EXTENSIONS
                        The file extension(s) to render (default: "py"). Separate multiple extensions with commas, or
                        use -e multiple times.
  --name FILES, -n FILES
                        The file name(s) to render. Separate multiple file names with commas, or use -n multiple
                        times.
  --version             show program's version number and exit
  -v {0,1,2,3}, --verbosity {0,1,2,3}
                        Verbosity level; 0=minimal output, 1=normal output, 2=verbose output, 3=very verbose output
  --settings SETTINGS   The Python path to a settings module, e.g. "myproject.settings.main". If this isn't provided,
                        the DJANGO_SETTINGS_MODULE environment variable will be used.
  --pythonpath PYTHONPATH
                        A directory to add to the Python path, e.g. "/home/djangoprojects/myproject".
  --traceback           Raise on CommandError exceptions
  --no-color            Don't colorize the command output.
  --force-color         Force colorization of the command output.

(djg329env) F:\WebProjects>

```

可以看到该命令有很多可选参数。我们目前就使用最简单的形式创建初始工程。

首先，cd 到一个你想放置工程代码的目录, 比如我们之前创建的代码仓库 ``django-tutorials``，然后运行以下命令：

```bash
(djg329env) F:\WebProjects>cd django-tutorials

(djg329env) F:\WebProjects\django-tutorials>django-admin startproject mysite

(djg329env) F:\WebProjects\django-tutorials>

```

这行代码将会在当前目录下创建一个 ``mysite`` 目录。 

	注解
	你得避免使用 Python 或 Django 的内部保留字来命名你的项目。具体地说，你得避免使用像 django (会和 Django 自己产生冲突)或 test (会和 Python 的内置组件产生冲突)这样的名字。


让我们看看 ``startproject`` 创建了些什么:

```bash
mysite/
    manage.py
    mysite/
        __init__.py
        settings.py
        urls.py
        asgi.py
        wsgi.py
```

上面这些 目录和文件就构成了一个Django项目的初始模板，每个文件的作用我们稍后会介绍。

### 2、使用 pycharm 创建项目

pycharm作为最常用的python集成开发环境，已经为我们提供了创建 django 项目的交互式图形界面，如下图所示：

![pycharm创建django项目](./images/pycharm-create-django.png)

使用pycharm创建的django项目的初始目录和文件如下：

```bash
mysite/
    manage.py
    mysite/
        __init__.py
        settings.py
        urls.py
        asgi.py
        wsgi.py
    templates\
``` 

与命令行方式创建的初始工程相比，这里多出了一个文件夹 ``templates`` , 并且在 `settings.py` 文件中会发现有一处不一样的地方，
如下所示：

```python
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates']  # 使用django-admin创建的初始工程，这里是个空列表
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
```

除了上面提到的不同点之外，这两种方式创建的初始工程一模一样。其实 pycharm 内部也是使用 ``django-admin``创建的初始工程，
然后又做了一点点补充。


## 二、初始项目各文件的用途

我们上面创建的初始工程模板的目录结构和包含的文件，如下所示：

![初始工程模板的目录结构和包含的文件](./images/django-initial-project.png)

这些文件的用途，下面解释：

- 最外层的 ``mysite/`` 根目录只是你项目的容器， 根目录名称对 ``Django`` 没有影响，你可以将它重命名为任何你喜欢的名称。

- ``manage.py``: 一个让你用各种方式管理 ``Django`` 项目的命令行工具。

- 里面一层的 ``mysite/`` 目录包含你的项目，它是一个纯 Python 包。它的名字就是当你引用它内部任何东西时需要用到的 Python 包名。 (比如 ``mysite.urls``).

- ``mysite/__init__.py``：一个空文件，告诉 Python 这个目录应该被认为是一个 Python 包。

- ``mysite/settings.py``：Django 项目的配置文件。

- ``mysite/urls.py``：Django 项目的 URL 声明，就像你网站的“目录”。

- ``mysite/asgi.py``：作为你的项目的运行在 ASGI 兼容的 Web 服务器上的入口。

- ``mysite/wsgi.py``：作为你的项目的运行在 WSGI 兼容的Web服务器上的入口。


## 三、运行初始项目

### 1、 命令行方式启动服务器

让我们来确认一下你的 Django 项目是否真的创建成功了。如果你的当前目录不是外层的 ``mysite`` 目录的话，请切换到此目录，然后运行下面的命令：

```bash
(djg329env) F:\WebProjects\django-tutorials\mysite>python manage.py runserver
```

你应该会看到如下输出：

```plaintext
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).

You have 18 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
November 20, 2021 - 12:43:24
Django version 3.2.9, using settings 'mysite.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CTRL-BREAK.
```

你还会看到根目录下面多了一个数据库文件 ``db.sqlite3`` 。而且，上面的输出信息中有关于迁移数据库的提示信息。
这时候先暂时忽略有关未应用最新数据库迁移的警告，稍后我们处理数据库。

你刚刚启动的是 Django 自带的用于开发的简易服务器，它是一个用纯 Python 写的轻量级的 Web 服务器。我们将这个服务器内置在 Django 中是为了让你能快速的开发出想要的东西，因为你不需要进行配置生产级别的服务器（比如 Apache）方面的工作，除非你已经准备好投入生产环境了。

现在是个提醒你的好时机：千万不要 将这个服务器用于和生产环境相关的任何地方。这个服务器只是为了开发而设计的。(我们在 Web 框架方面是专家，在 Web 服务器方面并不是。)

现在，服务器正在运行，浏览器访问 ``https://127.0.0.1:8000/`` 。 你将会看到一个“祝贺”页面，随着一只火箭发射，服务器已经运行了。

**更换端口**

默认情况下，``runserver`` 命令会将服务器设置为监听本机内部 IP 的 `8000` 端口。

如果你想更换服务器的监听端口，请使用命令行参数。举个例子，下面的命令会使服务器监听 8080 端口：

```bash
python manage.py runserver 8080
```

**修改监听IP**

如果你想要修改服务器监听的IP，在端口之前输入新的。比如，为了监听所有服务器的公开IP（这在你想要向网络上的其它电脑展示你的成果时很有用），使用：

```bash
python manage.py runserver 0.0.0.0:8080
# 或者
python manage.py runserver 0:8000   #  0 是 0.0.0.0 的简写
```

**会自动重新加载的服务器runserver**

用于开发的服务器在需要的情况下会对每一次的访问请求重新载入一遍 Python 代码。 所以你不需要为了让修改的代码生效而频繁的重新启动服务器。
然而，一些动作，比如添加新文件，将不会触发自动重新加载，这时你得自己手动重启服务器。

### 2、 pycharm方式启动服务器

![pycharm方式启动服务器](./images/pycharm-runserver.png)

以这种方式启动服务器，也可以修改服务端口。


## 四、将初始项目上传到码云仓库

在上传之前，我们需要修改一下 文件 ``.gitignore`` 文件的内容，把 数据库(db.sqlite3)也上传上去，如下：

```plaintext
....
....

# Django stuff:
*.log
local_settings.py
# db.sqlite3  把这个注释掉

...
...

```

还需要在 mysite 里面常见一个 文件夹 `docs` 用于存放一些说明文档。

最后，初始工程的目录结构如下所示：

```plaintext
mysite\
	docs\
		.keepstore
	mysite\
		__init__.py
		asgi.py
		settings.py
		urls.py
		wsgi.py
	templates\
		.keepstore
	db.sqlite3
	manage.py
```

``.keepstore``  是为了保证空文件夹也能被同步到代码仓库