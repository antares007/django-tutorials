# 表单的设计和数据处理视图

## 设计HTML表单页面

让我们更新一下在上一个教程中编写的投票详细页面的模板 ("polls/detail.html") ，让它包含一个 HTML <form> 元素：

```html
<form action="{% url 'polls:vote' question.id %}" method="post">
    {% csrf_token %}
    <fieldset>
        <legend><h1>{{ question.question_text }}</h1></legend>
        {% if error_message %}<p><strong>{{ error_message }}</strong></p>{% endif %}
        {% for choice in question.choice_set.all %}
        <input type="radio" name="choice" id="choice{{ forloop.counter }}" value="{{ choice.id }}">
        <label for="choice{{ forloop.counter }}">{{ choice.choice_text }}</label><br>
        {% endfor %}
    </fieldset>
    <input type="submit" value="Vote">
</form>
```

上面的表单是这个样子的：
![投票表单](./images/step007/s7img001.png)

简要说明：

- 上面的模板在 Question 的每个 Choice 前添加一个单选按钮。 每个单选按钮的 **value** 属性是对应的各个 
  Choice 的 ID。每个单选按钮的 **name** 是 **"choice"** 。这意味着，当有人选择一个单选按钮并提交表单提交时，
  它将发送一个 POST 数据 **choice=#** ，其中# 为选择的 Choice 的 ID。这是 HTML 表单的基本概念。
  
- 我们设置表单的 **action** 为 `{% url 'polls:vote' question.id %}` ，并设置 `method="post"` 。
使用 `method="post"` （与其相对的是 `method="get"`）是非常重要的，因为这个提交表单的行为会改变服务器端的数据。 
无论何时，当你需要创建一个改变服务器端数据的表单时，请使用 `method="post"` 。
  这不是 Django 的特定技巧；这是优秀的网站开发技巧。
  
- `forloop.counter` 指示 **for** 标签已经循环多少次。

- 由于我们创建一个 POST 表单（它具有修改数据的作用），所以我们需要小心 **跨站点请求伪造** 。 
  谢天谢地，你不必太过担心，因为 Django 自带了一个非常有用的防御系统。 
  简而言之，所有针对内部 URL 的 POST 表单都应该使用 `{% csrf_token %}` 模板标签。

## 编写处理表单的视图

现在，让我们来创建一个 Django 视图来处理提交的数据。

在前面，我们为投票应用创建了一个 `URLconf` ，包含这一行：

```python
path('<int:question_id>/vote/', views.vote, name='vote'),
```

我们还创建了一个 `vote()` 函数的虚拟实现。让我们来创建一个真实的版本。 将下面的代码添加到 `polls/views.py` ：

```python
def vote(request, question_id):
    """处理用户提交的投票表单数据的视图"""
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # 重新渲染表单
        return render(request, 'detail.html', {
            'question': question,
            'error_message': "你没有选择任何一个选项!"
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # 成功的处理完POST数据之后，总是要返回一个 HttpResponseRedirect 对象
        # 这可以防止用户提交两次同样的表单数据，如果他点击了返回键
        return HttpResponseRedirect(reverse('polls:results', args=(question.id, )))
```

以上代码中有些内容还未在本教程中提到过：

- **request.POST** 是一个类字典对象，让你可以通过关键字的名字获取提交的数据。 
  这个例子中， `request.POST['choice']` 以字符串形式返回选择的 Choice 的 ID。 
  `request.POST` 的值永远是字符串。
  
  注意，Django 还以同样的方式提供 `request.GET` 用于访问 `GET` 数据 —— 
  但我们在代码中显式地使用 `request.POST` ，以保证数据只能通过 `POST` 调用改动。
  
- 如果在 `request.POST['choice']` 数据中没有提供 `choice` ， `POST` 将引发一个 `KeyError` 。
上面的代码检查 `KeyError` ，如果没有给出 `choice` 将重新显示 **Question** 表单和一个错误信息。
  
- 在增加 `Choice` 的得票数之后，代码返回一个 `HttpResponseRedirect` 而不是常用的 `HttpResponse` 、 
  `HttpResponseRedirect` 只接收一个参数：用户将要被重定向的 URL。
  
- 在这个例子中，我们在 `HttpResponseRedirect` 的构造函数中使用 `reverse()` 函数。
  这个函数避免了我们在视图函数中硬编码 URL。
  它需要我们给出我们想要跳转的视图的名字和该视图所对应的 URL 模式中需要给该视图提供的参数。 
  在本例中，使用在 教程第 3 部分 中设定的 URLconf， **reverse()** 调用将返回一个这样的字符串：
  `'/polls/3/results/'`
  
其中 3 是 `question.id` 的值。重定向的 URL 将调用 'results' 视图来显示最终的页面。

## 展示投票结果的视图和模板

当有人对 Question 进行投票后， `vote()` 视图函数将请求重定向到 Question 的结果界面。让我们来编写这个视图：

```python
from django.shortcuts import get_object_or_404, render

def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html', {'question': question})
```

这和  `detail()` 视图几乎一模一样。唯一的不同是模板的名字。 我们将在稍后解决这个冗余问题。

现在，创建一个 `polls/results.html` 模板：

```html
<h1>{{ question.question_text }}</h1>
<ul>
  {% for choice in question.choice_set.all %}
      <li>{{ choice.choice_text }} -- {{ choice.votes }} vote{{ choice.votes|pluralize }}</li>
  {% endfor %}
</ul>
<a href="{% url 'polls:detail' question.id %}">继续投票?</a>
```

现在，在你的浏览器中访问 `/polls/1/` 然后为 Question 投票。
你应该看到一个投票结果页面，并且在你每次投票之后都会更新。 

![投票结果页面](./images/step007/s7img002.png)

如果你提交时没有选择任何 Choice，你应该看到错误信息。

## 通过Get请求提交表单

投票详细页面的模板 ("polls/detail.html") ，让它包含一个 HTML <form> 元素：

```html
<form action="{% url 'polls:vote' question.id %}" method="get">
    <fieldset>
        <legend><h1>{{ question.question_text }}</h1></legend>
        {% if error_message %}<p><strong>{{ error_message }}</strong></p>{% endif %}
        {% for choice in question.choice_set.all %}
        <input type="radio" name="choice" id="choice{{ forloop.counter }}" value="{{ choice.id }}">
        <label for="choice{{ forloop.counter }}">{{ choice.choice_text }}</label><br>
        {% endfor %}
    </fieldset>
    <input type="submit" value="Vote">
</form>
```

因为是GET请求，上面的表单没有 ``{% csrf_token %}`` ， 而且 `method="get"` 。

将下面的代码添加到 `polls/views.py` ：

```python
def vote(request, question_id):
    """处理用户提交的投票表单数据的视图"""
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.GET['choice'])
    except (KeyError, Choice.DoesNotExist):
        # 重新渲染表单
        return render(request, 'detail.html', {
            'question': question,
            'error_message': "你没有选择任何一个选项!"
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # 成功的处理完POST数据之后，总是要返回一个 HttpResponseRedirect 对象
        # 这可以防止用户提交两次同样的表单数据，如果他点击了返回键
        return HttpResponseRedirect(reverse('polls:results', args=(question.id, )))
```

提交表单的时候，访问的 URL 是这样的： ``/polls/8/vote/?choice=27`` ， 
可以看到 我们选中的选项是通过查询参数的形式提交到服务器的。
django 的视图访问这个参数的时候，用 ``request.GET['choice']`` 来获取。


## 限制未登录用户提交表单

### 原始方式

限制访问页面最原始的办法就是检查 `request.user.is_authenticated` 并重定向到登录页面。

```python
from django.conf import settings
from django.shortcuts import redirect

def my_view(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    # ...
```

或者显示一个错误信息：

```python
from django.shortcuts import render

def my_view(request):
    if not request.user.is_authenticated:
        return render(request, 'myapp/login_error.html')
```

### login_required 装饰器

``login_required(redirect_field_name='next', login_url=None)``

作为快捷方式，你可以使用 `login_required()` 装饰器:

```python
from django.contrib.auth.decorators import login_required

@login_required
def my_view(request):
    ...
```

`login_required()` 会执行以下操作：

- 如果用户没有登录，会重定向到 `settings.LOGIN_URL` ，并传递绝对路径到查询字符串中。例如： `/accounts/login/?next=/polls/3/` 。

- 如果用户已经登录，则正常执行视图。视图里的代码可以假设用户已经登录了。

默认情况下，成功验证时用户跳转的路径保存在名为 "next" 的查询字符串参数中。如果你希望这个参数使用不同名称，
请在 `login_required()` 中传递可选参数 `redirect_field_name` ：

```python
from django.contrib.auth.decorators import login_required

@login_required(redirect_field_name='my_redirect_field')
def my_view(request):
    ...
```

注意，如果你提供了 `redirect_field_name` 值，则很可能也需要自定义登录模板，因为存储重定向路径的模板上下文变量使用的是 `redirect_field_name` 值，
而不是 "next" （默认情况下）。

`login_required()` 也有可选参数 `login_url` 。例如：

```python
from django.contrib.auth.decorators import login_required

@login_required(login_url='/accounts/login/')
def my_view(request):
    ...
```
