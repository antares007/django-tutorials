# Django 自带的后台管理站点

Django 最强大的部分之一是自动管理界面。它从你的模型中读取元数据，提供一个快速的、以模型为中心的界面，受信任的用户可以管理你网站上的内容。
管理的推荐使用范围仅限于一个组织的内部管理工具。它不打算用于围绕你的整个前端构建。

管理有很多用于定制的钩子，但要注意不要试图专门使用这些钩子。如果你需要提供一个更以流程为中心的接口，抽象掉数据库表和字段的实现细节，
那么可能是时候编写自己的视图了。

在本文档中，我们将讨论如何激活、使用和定制 Django 的管理界面。

**设计哲学**

为你的员工或客户生成一个用户添加，修改和删除内容的后台是一项缺乏创造性和乏味的工作。因此，Django 全自动地根据模型创建后台界面。

Django 产生于一个 **公众页面** 和 **内容发布者页面** 完全分离的 **新闻类站点** 的开发过程中。

站点管理人员使用管理系统来添加新闻、事件和体育时讯等，这些添加的内容被显示在公众页面上。Django 通过为站点管理人员创建统一的内容编辑界面解决了这个问题。

管理界面不是为了网站的访问者，而是为管理者准备的。

我们之前创建的投票应用就符合这样一个站点特征： 投票发起人作为管理员在后台设置投票题目和选项， 然后被调查的公众则访问公众页面来投票。

接下来我们就把我们的投票应用注册到django的管理站点中。

## 一、启用后台管理站点

在 `startproject` 使用的默认项目模板中启用了后台管理站点。

如果你没有使用默认的项目模板，这里是要求：

(1) 将 'django.contrib.admin' 和它的依赖关系 —— `django.contrib.auth`、`django.contrib.contenttypes`、`django.contrib.messages` 
和 `django.contrib.session` 添加到你的 `INSTALLED_APPS` 配置中。当你在 `INSTALLED_APPS` 配置中放入 'django.contrib.admin' 时，
Django 会自动在每个应用程序中寻找 admin模块(就是 admin.py文件)并导入它。

(2) 在你的 `TEMPLATES` 配置中设置一个在 `OPTIONS` 的 'context_processors' 选项中包含 `django.template.context_processors.request`、
`django.contrib.auth.context_processors.auth` 和 `django.contrib.messages.context_processors.messages` 的 DjangoTemplates 后端：
Changed in Django 3.1: `django.template.context_processors.request` 被添加为 'context_processors' 选项中的依赖，
以支持新的 `AdminSite.enable_nav_sidebar`。

(3) 如果你自定义了 `MIDDLEWARE` 设置，则必须包含 `django.contrib.auth.middleware.AuthenticationMiddleware` 和 
`django.contrib.messages.middleware.MessageMiddleware` 。

(4) 把后台管理站点的网址挂到你的 `URLconf` 里。

在你采取了这些步骤之后，你就可以通过访问你挂接的 URL（默认是 `/admin/`）来使用管理站点。

如果需要创建一个用户来登录，请使用 `createsuperuser` 命令。默认情况下，登录管理需要用户的 `is_staff` 属性设置为 True。

最后，确定你的应用程序的哪些模型应该在管理界面中是可编辑的。对于这些模型中的每一个，按照 ModelAdmin 中的描述，在管理处注册它们。



## 一、创建一个管理员账号

首先，我们得创建一个能登录管理页面的用户。请运行下面的命令：

```bash
python manage.py createsuperuser
```

键入你想要使用的用户名(回车), 邮件地址(回车)，密码(回车)，如下所示：

```bash
manage.py@mysite > createsuperuser
Tracking file by folder pattern:  migrations
用户名 (leave blank to use 'administrator'):  admin
电子邮件地址:  admin@mysite.com
Warning: Password input may be echoed.
Password:  qaz.wsx.123
Warning: Password input may be echoed.
Password (again):  qaz.wsx.123
Superuser created successfully.

Process finished with exit code 0
```

创建完超级管理者用户之后，我们看到数据表 ``auth_user`` 多了一个用户，如下所示：

![auth_user表中的用户](./images/step005/s5img001.png)

上表中每一字段的意义必须要搞清楚：


## 二、启动开发服务器

Django 的管理界面默认就是启用的。让我们启动开发服务器，看看它到底是什么样的。

如果开发服务器未启动，用以下命令启动它：

```bash
python manage.py runserver
```

现在，打开浏览器，转到你本地域名的 "/admin/" 目录， -- 比如 "http://127.0.0.1:8000/admin/" 。你应该会看见管理员登录界面：

![django管理站点登录界面](./images/step005/s5img002.png)

因为 翻译功能默认是开启的(`USE_I18N = True`)，如果你设置了 `LANGUAGE_CODE`，登录界面将显示你设置的语言（如果 Django 有相应的翻译）。

## 三、进入管理站点页面
现在，试着使用你在上一步中创建的超级用户来登录。然后你将会看到 Django 管理页面的索引页：

![django管理站点初始界面](./images/step005/s5img003.png)

你将会看到几种可编辑的内容：组和用户。它们是由 `django.contrib.auth` 提供的，这是 Django 开发的认证框架。

## 四、向管理页面中加入投票应用

但是我们的投票应用在哪呢？它没在索引页面里显示。

只需要再做一件事：我们得告诉管理站点，问题 **Question** 对象需要一个后台接口。打开 `polls/admin.py` 文件，把它编辑成下面这样：

```python
from django.contrib import admin
from .models import Question, Choice
admin.site.register(Question)
admin.site.register(Choice)
```

然后，刷新后台站点的首页面，就可以看到我们的 `polls` 应用已经被注册到站点中了：

![django管理站点初始界面](./images/step005/s5img004.png)

现在我们向管理页面注册了问题(Question) 类 和 选项(Choice)类 。Django 知道它应该被显示在索引页里。

## 五、 体验便捷的管理功能

点击 "Questions" 。现在看到是问题 "Questions" 对象的列表 "change list" 。这个界面会显示所有数据库里的问题 Question 对象，你可以选择一个来修改。
这里现在有我们在上一部分中创建的 “What's up?” 问题。

![问题 "Questions" 对象的列表页](./images/step005/s5img005.png)

点击 “What's up?” 来编辑这个问题（Question）对象：

![问题（Question）对象的修改页](./images/step005/s5img006.png)

**注意事项：**

- 这个表单是从问题 **Question** 模型中自动生成的

- 不同的字段类型（日期时间字段 `DateTimeField` 、字符字段 `CharField`）会生成对应的 HTML 输入控件。每个类型的字段都知道它们该如何在管理页面里显示自己。

- 每个日期时间字段 `DateTimeField` 都有 `JavaScript` 写的快捷按钮。日期有转到今天（Today）的快捷按钮和一个弹出式日历界面。
  时间有设为现在（Now）的快捷按钮和一个列出常用时间的方便的弹出式列表。
  
页面的底部提供了几个选项：

- 保存（Save） - 保存改变，然后返回对象列表。
- 保存并继续编辑（Save and continue editing） - 保存改变，然后重新载入当前对象的修改界面。
- 保存并新增（Save and add another） - 保存改变，然后添加一个新的空对象并载入修改界面。
- 删除（Delete） - 显示一个确认删除页面。

如果显示的 “发布日期(Date Published)” 和你在 教程 1 里创建它们的时间不一致，这意味着你可能没有正确的设置 TIME_ZONE 。
改变设置`USE_TZ = True`，然后重新载入页面看看显示时间是否会发生变化。

通过点击 “今天(Today)” 和 “现在(Now)” 按钮改变 “发布日期(Date Published)”。然后点击 “保存并继续编辑(Save and add another)”按钮。
然后点击右上角的 “历史(History)”按钮。你会看到一个列出了所有通过 Django 管理页面对当前对象进行的改变的页面，其中列出了时间戳和进行修改操作的用户名：

![问题（Question）对象的修改历史页](./images/step005/s5img007.png)

## 六、为应用和模型添加元信息

我们看到上面的 应用(polls) 和 模型(Question, Choice) 都显示的是英语，为了在后台站点显示对应的中文，我们需要添加 **元信息** 。

首先，修改 ``polls/apps.py`` 文件，添加 `verbose_name` 属性：

```python
from django.apps import AppConfig


class PollsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'polls'
    verbose_name = '投票应用'

```

接着，给 ``polls/models.py`` 文件中的 Question类 和 Choice类 分别添加 Meta 信息， 如下所示：

```python
import datetime

from django.db import models
from django.utils import timezone

class Question(models.Model):
    question_text = models.CharField(verbose_name="问题内容", max_length=200)
    pub_date = models.DateTimeField(verbose_name='发布时间')

    class Meta:
        verbose_name = "问题"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

class Choice(models.Model):
    question = models.ForeignKey(Question, verbose_name='所属问题', on_delete=models.CASCADE)
    choice_text = models.CharField(verbose_name='选项内容', max_length=200)
    votes = models.IntegerField(verbose_name='投票数量', default=0)

    class Meta:
        verbose_name = "选项"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.choice_text

```

在模型的 元信息(Meta) 和 字段(Field) 添加了 ``verbose_name`` 之后，后台管理站点的模型名称和字段名称 就变为 中文 了，

![模型名称 就变为 中文](./images/step005/s5img008.png)

![Question的字段 就变为 中文](./images/step005/s5img009.png)

![Choice的字段 就变为 中文](./images/step005/s5img010.png)

## 7、列表页显示多个属性字段信息

因为 ``polls/models.py`` 文件中的 Question类 的魔法函数 `__str()__` 返回的是 `question_text` 这个字段，所以后台站点的列表页显示的就是
每个问题的问题内容文本。如果想让后台站点的列表页显示的是 该数据项在数据库中的 ID 字段，则把 `__str()__` 改为以下内容，

```python
def __str__(self):
        return self.id
```

![模型列表页显示每个数据项在数据库中的ID](./images/step005/s5img011.png)

如果既要显示 `id` 字段，又要显示 `question_text` 字段, 还要显示 `pub_date` 字段，该怎么做呢？

这时候，需要用到 模型注册类 `ModelAdmin` ，使用方法是去重新修改 `polls/admin.py` , 如下所示：

```python
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from .models import Question, Choice

class QuestionAdmin(ModelAdmin):
    list_display = ['id', 'question_text', 'pub_date']

class ChoiceAdmin(ModelAdmin):
    list_display = ['id', 'choice_text', 'votes', 'question']

admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice, ChoiceAdmin)
```

后台站点的显示效果如下所示：

![显示Question的多个属性](./images/step005/s5img012.png)

![显示Choice的多个属性](./images/step005/s5img013.png)


## 8、ModelAdmin的更多配置选项

`ModelAdmin` 非常灵活。它有几个选项用于处理自定义界面。所有选项都在 `ModelAdmin` 子类中定义：

### ModelAdmin.date_hierarchy

将 `date_hierarchy` 设置为你的模型中 `DateField` 或 `DateTimeField` 的名称，变化列表页将包括一个基于日期的向下扩展。

举例：
```python
date_hierarchy = 'pub_date'  # 在 QuestionAdmin 上加入该配置项
```
你也可以使用 `__` 查找来指定相关模型上的字段，例如：
```python
date_hierarchy = 'question__pub_date' # 在 ChoiceAdmin 类中加入这个配置项
```
这将根据可用的数据智能地填充自己，例如，如果所有的日期都在一个月内，它将只显示日级的向下扩展。

![层级时间过滤器](./images/step005/s5img014.png)

### ModelAdmin.empty_value_display

该属性覆盖记录字段为空（None、空字符串等）的默认显示值。默认值是 - （破折号）。例如：

```python
from django.contrib import admin

class QuestionAdmin(admin.ModelAdmin):
    empty_value_display = '-empty-'
```

你也可以用 `AdminSite.empty_value_display` 覆盖所有管理页面的 `empty_value_display`，或者像这样覆盖特定字段：

```python
from django.contrib import admin

class AuthorAdmin(admin.ModelAdmin):
    fields = ('name', 'title', 'view_birth_date')

    @admin.display(empty_value='???')
    def view_birth_date(self, obj):
        return obj.birth_date
```

### ModelAdmin.exclude
如果给定了这个属性，那么这个属性应该是一个要从表单中排除的字段名列表。

例如，我们考虑以下模型：
```python
from django.db import models

class Author(models.Model):
    name = models.CharField(max_length=100)
    title = models.CharField(max_length=3)
    birth_date = models.DateField(blank=True, null=True)
```

如果你想让 `Author` 模型的表格只包括 `name` 和 `title` 字段，你可以指定 `fields` 或 `exclude` ，如：
```python
from django.contrib import admin

class AuthorAdmin(admin.ModelAdmin):
    fields = ('name', 'title')

class AuthorAdmin(admin.ModelAdmin):
    exclude = ('birth_date',)
```

由于 `Author` 模型只有三个字段，即 `name`、`title` 和 `birth_date`，因此上述声明所产生的表单将包含完全相同的字段。

### ModelAdmin.list_display_links

使用 `list_display_links` 来控制 `list_display` 中的字段是否以及哪些字段应该被链接到对象的 “更改” 页面。

默认情况下，更改列表页将把第一列 —— `list_display` 中指定的第一个字段 —— 链接到每个项目的更改页面。但是 `list_display_links` 让你改变这一点：

- 将其设置为 `None`，则完全没有链接。

- 将它设置为一个列表或元组字段（格式与 `list_display` 相同），你希望将其列转换为链接。

### ModelAdmin.list_editable

将 `list_editable` 设置为模型上允许在更改列表页上编辑的字段名称列表。也就是说，在 `list_editable` 中列出的字段将作为表单部件显示在变更列表页上，
允许用户一次编辑和保存多行。

- `list_editable` 中的任何字段都必须在 `list_display` 中。你不能编辑一个没有显示的字段！

- 同一字段不能同时列在 `list_editable` 和 `list_display_links` 中 —— 一个字段不能既是表单又是链接。

如果这些规则中的任何一条被破坏，你会得到一个验证错误。

### ModelAdmin.list_filter

设置 `list_filter` 来激活管理更改列表页面右侧侧栏的过滤器，

```python
class QuestionAdmin(ModelAdmin):
    date_hierarchy = 'pub_date'
    list_display = ['id', 'question_text', 'pub_date']
    list_display_links = ['id', 'question_text',]  # None
    # list_editable = ['pub_date']
    list_filter = ['pub_date']  # 使用日期字段做过滤器
```

如下截图所示：

![使用日期字段做过滤器](./images/step005/s5img015.png)

`list_filter` 应是一个元素的列表或元组，其中每个元素应是下列类型之一：

- 一个字段名，其中指定的字段应该是 `BooleanField`、`CharField`、`DateField`、`DateTimeField`、`IntegerField`、`ForeignKey` 或 `ManyToManyField`。

### ModelAdmin.list_per_page

设置 `list_per_page` 来控制每个分页的管理变更列表页面上出现多少个项目。默认情况下，设置为 100。

### ModelAdmin.ordering

设置 `ordering` 来指定对象列表在 Django 管理视图中的排序方式。这应该是一个列表或元组，格式与模型的 `ordering` 参数相同。

如果没有提供，Django 管理员将使用模型的默认排序。

如果你需要指定一个动态的顺序（例如取决于用户或语言），你可以实现一个 `get_ordering()` 方法。

### ModelAdmin.autocomplete_fields

`autocomplete_fields` 是一个 `ForeignKey` 和／或 `ManyToManyField` 字段的列表，你想 自动完成输入。

`autocomplete_fields` 是一个 `ForeignKey` 和／或的列表，默认情况下，管理对这些字段使用选择框接口（<select>）。
有时你不想产生选择所有相关实例在下拉中显示的开销。ManyToManyField 字段你想改成 Select2 自动完成输入。

Select2 输入看起来与默认输入类似，但自带搜索功能，异步加载选项。如果相关模型有很多实例，这样会更快、更方便用户使用。

你必须在相关对象的 `ModelAdmin` 上定义 `search_fields`，因为自动完成搜索使用它。

为了避免未经授权的数据泄露，用户必须拥有相关对象的 view 或 change 权限才能使用自动完成。

结果的排序和分页由相关的 `ModelAdmin` 的 `get_ordering()` 和 `get_paginator()` 方法控制。

在下面的例子中，`ChoiceAdmin` 对 `Question` 有一个 `ForeignKey` 的自动完成字段。结果由 `question_text` 字段过滤，并由 `date_created` 字段排序：

```python
class QuestionAdmin(ModelAdmin):
    ordering = ['pub_date']
    search_fields = ['question_text']

class ChoiceAdmin(ModelAdmin):
    autocomplete_fields = ['question']
```

联想补全，自动完成的输入框效果：

![自动完成的输入框效果](./images/step005/s5img016.png)

### ModelAdmin.readonly_fields

默认情况下，管理会将所有字段显示为可编辑。该选项中的任何字段（应该是 list 或 tuple）将按原样显示其数据，不可编辑；

```python
class ChoiceAdmin(ModelAdmin):
    readonly_fields = ['question']
```


## 综合一下上面讲过的配置参数

```python
class QuestionAdmin(ModelAdmin):
    date_hierarchy = 'pub_date'
    list_display = ['id', 'question_text', 'pub_date']
    list_display_links = ['id', 'question_text',]  # None
    # list_editable = ['pub_date']
    list_filter = ['pub_date']
    # list_per_page = 8
    ordering = ['pub_date', ]
    search_fields = ['question_text']


class ChoiceAdmin(ModelAdmin):
    date_hierarchy = 'question__pub_date'
    list_display = ['id', 'choice_text', 'votes', 'question']
    list_display_links = ['id', 'choice_text']  # None
    list_editable = ['votes']
    # list_filter = ['votes']
    # list_per_page = 25
    autocomplete_fields = ['question']
    readonly_fields = ['question']
```