# Django的配置文件详解

Django 的主要部署平台是 WSGI，它是 Web 服务器和 Web 应用的 Python 标准。

Django 的配置文件包含 Django 应用的所有配置项。
Django 的管理命令 ``startproject`` 生成了一个最小化的默认 WSGI 配置(`settings.py`)。
你可以按照自己项目的需要去调整这个配置。

任何兼容 WSGI 的应用程序服务器(runserver, gunicorn, uWSGI等)都可以直接使用。

## 一、配置文件是个什么东西

配置文件只是一个使用模块级变量的一个 Python 模块(.py文件)。

一组配置例子:
```python
BASE_DIR = Path(__file__).resolve().parent.parent
DEBUG = True
ALLOWED_HOSTS = []
ROOT_URLCONF = 'mysite.urls'
WSGI_APPLICATION = 'mysite.wsgi.application'
```

因为配置文件是一个 Python 模块，所以要注意以下几项:
- 不能有 Python 语法错误 
- 可以用 Python 语法实现动态配置，例如: ``MY_SETTING = [str(i) for i in range(30)]``
- 可以从其他配置文件中引入变量

## 二、配置文件是如何生效的

这个问题也可以这么问： 配置文件是如何被WSGI服务器使用的？

### 1、wsgi application 对象

用 WSGI 部署的关键是 application callable，应用服务器(比如 runserver)用它与你的代码交互。 

application callable 一般位于一个 Python 模块中，名为 application 的对象的形式提供，且对服务器可见。

`startproject` 命令创建了文件 ``<project_name>/wsgi.py``，其中包含了 application callable， 如下所示：

```python
import os
from django.core.wsgi import get_wsgi_application
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mysite.settings')
application = get_wsgi_application()
```

Django 自带的开发服务器(runserver)和生产环境的 WSGI 部署(如gunicorn,uWSGI)都用到了上面的 `application` 对象。

WSGI 服务器从其配置(`settings.py`)中获取 application callable 的路径。
Django 的默认服务器（runserver 命令），从配置项 `WSGI_APPLICATION` 中获取。
默认值是 `<project_name>.wsgi.application`，指向 ``<project_name>/wsgi.py`` 中的 application callable。

### 2、使用环境变量来指定配置文件

当你使用 Django 的时候，在服务器环境下，你必须告诉WSGI的application，它应该使用哪个配置文件。
这可以通过使用环境变量 `DJANGO_SETTINGS_MODULE` 来实现。

上面的文件 ``<project_name>/wsgi.py`` 的代码中，有一行代码是这样的： 
```python
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mysite.settings')
# 因为 os.environ 是个字典，所以也可以写成下面的形式
# os.environ['DJANGO_SETTINGS_MODULE'] = 'mysite.settings'  
```

`DJANGO_SETTINGS_MODULE` 的值是一个符合 Python 语法的路径，例如 `mysite.settings`。
要注意配置模块应位于 Python 的 import 搜索路径 中。

若未设置该变量， wsgi.py 默认将其设置为 `mysite.settings`， ``mysite`` 即工程名字。
这就是 ``runserver`` 如何默认的发现默认配置的。

## 三、系统默认配置文件在哪里

在无需配置的情况下，Django配置文件没必要定义任何配置项。因为每个配置项都有一个明确的默认值。

默认值都保存在模块 ``django/conf/global_settings.py`` 中（可以使用pycharm的快捷操作快速找到它）。

Django按如下算法编译配置模块:

- 首先，从 `global_settings.py` 中加载全局配置项。
- 接着，从指定的配置文件中加载配置项，覆盖对应的全局配置项。

注意: 配置文件中不要再重复引用 `global_settings`，因为这样做是多余的。

**查看你已经更改了哪个配置项**

```bash
python manage.py diffsettings # 显示当前配置文件与 Django 全局默认配置的差异。
```
执行上述命令，就可以得到 已经被修改过的配置项有哪些：
```bash
manage.py@mysite > diffsettings
D:\Programs\JetBrains\PyCharm\bin\runnerw64.exe ......
Tracking file by folder pattern:  migrations
AUTH_PASSWORD_VALIDATORS = [{'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'}, {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'}, {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'}, {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'}]
BASE_DIR = WindowsPath('F:/WebProjects/django-tutorials/mysite')  ###
DATABASES = {'default': {'ENGINE': 'django.db.backends.sqlite3', 'NAME': WindowsPath('F:/WebProjects/django-tutorials/mysite/db.sqlite3'), 'ATOMIC_REQUESTS': False, 'AUTOCOMMIT': True, 'CONN_MAX_AGE': 0, 'OPTIONS': {}, 'TIME_ZONE': None, 'USER': '', 'PASSWORD': '', 'HOST': '', 'PORT': '', 'TEST': {'CHARSET': None, 'COLLATION': None, 'MIGRATE': True, 'MIRROR': None, 'NAME': None}}}
DEBUG = True
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
INSTALLED_APPS = ['django.contrib.admin', 'django.contrib.auth', 'django.contrib.contenttypes', 'django.contrib.sessions', 'django.contrib.messages', 'django.contrib.staticfiles']
MIDDLEWARE = ['django.middleware.security.SecurityMiddleware', 'django.contrib.sessions.middleware.SessionMiddleware', 'django.middleware.common.CommonMiddleware', 'django.middleware.csrf.CsrfViewMiddleware', 'django.contrib.auth.middleware.AuthenticationMiddleware', 'django.contrib.messages.middleware.MessageMiddleware', 'django.middleware.clickjacking.XFrameOptionsMiddleware']
ROOT_URLCONF = 'mysite.urls'  ###
SECRET_KEY = 'django-insecure-nj^&yx9=-iy(u78x=7ndsh6i_+gded!g(b$go0r##4va_sh7ad'
SETTINGS_MODULE = 'mysite.settings'  ###
STATIC_URL = '/static/'
TEMPLATES = [{'BACKEND': 'django.template.backends.django.DjangoTemplates', 'DIRS': [WindowsPath('F:/WebProjects/django-tutorials/mysite/templates')], 'APP_DIRS': True, 'OPTIONS': {'context_processors': ['django.template.context_processors.debug', 'django.template.context_processors.request', 'django.contrib.auth.context_processors.auth', 'django.contrib.messages.context_processors.messages']}}]
TIME_ZONE = 'UTC'
USE_L10N = True
USE_TZ = True
WSGI_APPLICATION = 'mysite.wsgi.application'

Process finished with exit code 0
```

## 四、在 Python 代码中使用 settings

在具体的Django应用中, 通过引入 ``django.conf.settings`` 使用配置, 例:

```python
from django.conf import settings

if settings.DEBUG:
    # Do something
```
注意 `django.conf.settings` 并不是一个模块！-- 而是一个对象！所以引入某个单独的设置项是不可能的:
```python
from django.conf.settings import DEBUG  # This won't work.
```

还要注意 不应该 **直接引入** `global_settings` 和你自己的配置文件。
`django.conf.settings` 整合了默认配置和网站指定配置，从而只提供一个接口读取配置。 它也解耦了使用自定义配置文件的代码。

比如我们要在 ``mysite/urls.py`` 文件中使用某一个配置项，如下所示：
```python
from django.conf import settings # 此处的 settings 是个对象, 正确用法
# from mysite import settings  # 此处的 settings 是个模块，不要直接引入
from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),
]

# 下面使用了 DEBUG 配置项
if settings.DEBUG:  
    urlpatterns += [path('', views.home, name='home') ]
```

**不建议在运行时更改设置，应该只在 settings 文件中更改配置项。**

比如, 不要在view中这样用:
```python
from django.conf import settings
settings.DEBUG = True   # 不要这么做
```

## 五、常用配置项的用途与设置方法

### 1、配置项 LANGUAGE_CODE ：

代表本次安装的语言代码的字符串。这应该是标准的 language ID 格式。例如，美国英语是 "en-us"。 该配置项的默认值是： 'en-us' 。

完整的语言标识列表可以在 Django的全局配置文件 ``global_settings.py`` 中找到。简体中文的标识是 `zh-hans` 。

`USE_I18N=True` 的时候，该配置才会有效果。

### 2、配置项 USE_I18N, USE_L10N：
国际化和本地化的目标是让同一站点为不同的用户提供定制化的语言和格式服务。

Django 完整支持 翻译文本， 格式化日期，时间和数字 以及 时区。

实际上，Django 做了两件事：

- 它允许开发者和模板作者指定应用的哪个部分应该被翻译或格式化为本地语言，符合本地文化。
- 它根据用户的配置利用钩子本地化 Web 应用。

翻译依赖于目标语言，而格式化规则依赖于目标国家。此信息由浏览器的 Accept-Language 头提供。但是，时区并不容易获得。

国际化（internationalization）和本地化（localization）总是令人迷惑；以下是个简单的定义：

**国际化：** 为本地化准备软件。通常由开发者完成。

**本地化：** 编写翻译和本地格式化。通常由翻译者完成。

翻译和格式化分别由 `USE_I18N` 和 `USE_L10N` 控制。

**USE_I18N**

一个布尔值，用于指定是否应该启用 Django 的翻译系统。这提供了一个关闭翻译系统的方法，以保证性能。如果设置为 `False`，Django 会进行一些优化，
以避免加载翻译机制。默认的 `settings.py` 文件由 `django-admin startproject` 创建，默认设置了 `USE_I18N = True`。

如果配置项如下, 则：
```python
LANGUAGE_CODE = 'zh-hans'
USE_I18N = True # 若开启翻译系统，则Django的火箭欢迎页面会显示中文
USE_I18N = False # 若关闭翻译系统，就算LANGUAGE_CODE设为中文，Django的火箭欢迎页面还是会显示English
```

**USE_L10N**

一个布尔值，用于指定是否默认启用数据的本地化格式。如果设置为 `True`，例如，Django 将使用当前语言的格式来显示数字和日期。
默认的 `settings.py` 文件由 `django-admin startproject` 创建，默认设置了 `USE_L10N = True`。

### 3、配置项 TIME_ZONE, USE_TZ：

**USE_TZ** ： 一个布尔值，用于指定 Django 是否默认使用时区感知。如果设置为 True，Django 将在内部使用时区感知的日期。
如果设置为 False ，Django 将使用本地时间的本地日期。

**TIME_ZONE**：表示本次安装的时区的字符串 。DATABASES 配置的这个内部选项与一般的 TIME_ZONE 配置接受相同的值。

当 `USE_TZ` 为 `True` 且设置了这个选项时，从数据库中读取日期时间会返回这个时区的感知日期时间，而不是 UTC。
当 `USE_TZ` 为 False 时，设置该选项是错误的。

- 如果数据库后端不支持时区（如 SQLite、MySQL、Oracle），如果设置了这个选项，Django 会根据这个选项以当地时间读写日期，如果没有设置，则以 UTC 时间读写。

- 如果数据库后端支持时区（如 PostgreSQL），很少需要 TIME_ZONE 选项。它可以在任何时候改变；数据库会负责将日期时间转换为所需的时区。

在 ``django.utils.timezone``  里面有一个 `now()` 函数可以返回当前时间， 源码如下所示：

```python
import pytz
from datetime import datetime, timedelta, timezone, tzinfo
from django.conf import settings
# UTC time zone as a tzinfo instance.
utc = pytz.utc
def now():
    """
    Return an aware or naive datetime.datetime, depending on settings.USE_TZ.
    """
    if settings.USE_TZ:
        # timeit shows that datetime.now(tz=utc) is 24% slower
        return datetime.utcnow().replace(tzinfo=utc)
    else:
        return datetime.now()
```

一般来说，上面这两个选项可以配置如下：
```python
USE_TZ = False # 关闭自动时区感知，使用本地时区
TIME_ZONE = 'Asia/Shanghai'  # 中国时区的设置方法
```

### 4、配置项 WSGI_APPLICATION

Django 内置服务器（如 runserver）将使用的 WSGI 应用对象的完整 Python 路径。

```python
WSGI_APPLICATION = 'mysite.wsgi.application'
```

`django-admin startproject` 管理命令将创建一个标准的 `wsgi.py` 文件，其中有一个 `application` 可调用对象，并将此配置指向该 `application`。

```python
import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ShangDaXue.settings')

application = get_wsgi_application()
```

如果没有设置，将使用 `django.core.wsgi.get_wsgi_application()` 的返回值。在这种情况下， runserver 的行为将与之前的 Django 版本相同。

### 5、配置项 ROOT_URLCONF

一个字符串，代表你的根 URLconf 的完整 Python 导入路径，可以通过在传入的 HttpRequest 对象上设置属性 urlconf 来覆盖每个请求。

这个配置项是为了告诉系统 你的根路由的配置文件是哪一个，一般默认就是 ``mysite/urls.py``， 所以配置项的写法就是

```python
ROOT_URLCONF = 'mysite.urls'  # 'mysite' 是工程目录中与顶级的根目录具有相同名称的包名，而不是顶级的根目录
```

### 6、配置项 AUTH_PASSWORD_VALIDATORSS

用于检查用户密码强度的验证器列表。

验证由 `AUTH_PASSWORD_VALIDATORS` 控制。默认的设置是一个空列表，这意味着默认是不验证的。 

在使用默认的 `django-admin startproject` 创建的新项目中，默认启用了验证器集合, 如下所示：

```python
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 9,    #  限定密码最小长度
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
```

上述配置项启用了所有包含的验证器：

`UserAttributeSimilarityValidator` 检查密码和一组用户属性集合之间的相似性。 

`MinimumLengthValidator` 用来检查密码是否符合最小长度。这个验证器可以自定义设置：它现在需要最短9位字符，而不是默认的8个字符。 

`CommonPasswordValidator` 检查密码是否在常用密码列表中。默认情况下，它会与列表中的2000个常用密码作比较。 

`NumericPasswordValidator` 检查密码是否是完全是数字的。 

对于 `UserAttributeSimilarityValidator` 和 `CommonPasswordValidator` ，我们在这个例子里使用默认配置。 
`NumericPasswordValidator` 不需要设置。

帮助文本和来自密码验证器的任何错误信息始终按照 `AUTH_PASSWORD_VALIDATORS` 列出的顺序返回。

### 7、配置项 MIDDLEWARE 

中间件是 Django 请求/响应处理的钩子框架。它是一个轻量级的、低级的“插件”系统，用于全局改变 Django 的输入或输出。

每个中间件组件负责做一些特定的功能。例如，Django 包含一个中间件组件 ``AuthenticationMiddleware``，它使用会话(session)将用户(user)与请求(request)关联起来。

中间件是如何工作的，如何激活中间件，以及如何编写自己的中间件。Django 具有一些内置的中间件，你可以直接使用。

若要激活中间件组件，请将其添加到 Django 设置中的 ``MIDDLEWARE`` 列表中。

在 ``MIDDLEWARE`` 中，每个中间件组件由字符串表示：指向中间件工厂的类或函数名的完整 Python 路径。

例如，这里 ``django-admin startproject`` 创建的默认值是 ：

```python
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
```

Django 安装不需要任何中间件——如果您愿意的话，``MIDDLEWARE`` 可以为空——但是强烈建议您至少使用 ``CommonMiddleware``。

``MIDDLEWARE`` 的顺序很重要，因为中间件会依赖其他中间件。例如：类 ``AuthenticationMiddleware`` 在会话中存储经过身份验证的用户； 
因此，它必须在 ``SessionMiddleware`` 后面运行 。

**中间件顺序与分层**

在请求阶段，在调用视图之前，Django 按照定义的顺序应用中间件 ``MIDDLEWARE``，自顶向下。

你可以把它想象成一个**洋葱**：每个中间件类都是一个“层”，它覆盖了洋葱的核心。如果请求通过洋葱的所有层（每一个调用 `get_response` ）以将请求传递到下一层，
一直到内核的视图，那么响应将在返回的过程中通过每个层（以相反的顺序）。

如果其中一层决定停止并返回响应而不调用`get_response`，那么该层(包括视图)中的洋葱层都不会看到请求或响应。响应将只通过请求传入的相同层返回。

### 8、配置项 INSTALLED_APPS 

一个字符串的列表，表示在这个 Django 安装中所有**被启用**的应用程序(app)。每一个字符串都应该是一个 Python 的点分隔路径。

    - 应用程序配置类（首选）
    
    - 包含应用程序的包。

例如，这里 ``django-admin startproject`` 创建的默认值是 ：

```python
# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',             #   管理员站点
    'django.contrib.auth',              #   认证授权系统
    'django.contrib.contenttypes',      #   内容类型框架
    'django.contrib.sessions',          #   会话框架
    'django.contrib.messages',          #   消息框架
    'django.contrib.staticfiles',       #   管理静态文件的框架
]
```

使用应用程序注册进行自省

你的代码不应该直接访问 `INSTALLED_APPS`。使用 `django.apps.apps` 代替。

在 `INSTLED_APPS` 中，应用程序名称和标签必须是唯一的。

应用程序 **名称** —— 指向应用程序包的点分隔 Python 路径——必须是唯一的。没有办法将同一个应用程序包含两次，除非用另一个名字复制它的代码。

应用程序 **标签** —— 默认情况下，名称的最后一部分也必须是唯一的。例如，你不能同时包含 `django.contrib.auth` 和 `myproject.auth`。 
但是，你可以用自定义配置重新标注一个应用程序，定义不同的 `label`。

无论 `INSTALLED_APPS` 引用的是应用程序配置类还是应用包，这些规则都适用。

### 9、配置项 DEBUG

一个开启、关闭调试模式的布尔值。

永远不要在 `DEBUG` 开启的情况下将网站部署到生产中。

调试模式的主要功能之一是显示详细的错误页面。如果你的应用程序在 `DEBUG` 为 `True` 时引发了异常，Django 会显示一个详细的回溯，
包括很多关于你的环境的元数据，比如所有当前定义的 Django 配置（来自 settings.py）。

请注意，你的调试输出中总会有一些部分是不适合公开的。文件路径、配置选项等都会给攻击者提供关于你的服务器的额外信息。

同样重要的是，当 DEBUG 开启时，Django 会记住它执行的每个 SQL 查询。这在调试时很有用，但在生产服务器上会迅速消耗内存。

最后，如果 `DEBUG` 为 False，还需要正确设置 `ALLOWED_HOSTS` 配置。否则，所有的请求都会以 “Bad Request (400) ” 返回。

默认的 `settings.py` 文件由 `django-admin startproject` 创建，为了方便，设置 `DEBUG = True`。

### 10、配置项 SECRET_KEY

一个特定 Django 安装的密钥。用于提供 **加密签名**，并且应该设置为一个唯一的、不可预测的值。

`django-admin startproject` 自动为每个新项目添加一个随机生成的 `SECRET_KEY` ， 如下所示：

```python
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-nj^&yx9=-iy(u78x=7ndsh6i_+gded!g(b$go0r##4va_sh7ad'
```

将此值保密。在已知的 SECRET_KEY 的情况下运行 Django，会破坏 Django 的许多安全保护措施，并可能导致权限升级和远程代码执行漏洞。

键的使用不应该假设是文本或字节。每次使用都应该通过 force_str() 或 force_bytes() 将其转换为所需类型。

如果 SECRET_KEY 没有设置，Django 将拒绝启动。

警告

### 11、配置项 ALLOWED_HOSTS

一个代表这个 Django 网站可以服务的主机／域名的字符串列表。这是一个安全措施，以防止 HTTP 主机头攻击 ，即使在许多看似安全的 Web 服务器配置下也有可能发生。

这个列表中的值可以是完全限定的名称（例如 'www.example.com'），在这种情况下，它们将与请求的 Host 头完全匹配（不区分大小写，不包括端口）。

以英文句号开头的值可以用作子域通配符。'.example.com' 将匹配 'example.com'、'www.example.com' 和 'example.com' 的任何其他子域。

'*' 的值将匹配任何东西；在这种情况下，你要负责提供你自己的 Host 头的验证（也许是在一个中间件中；如果是这样，这个中间件必须首先在 MIDDLEWARE 中列出）。

当 `DEBUG`为`True` 和 `ALLOWED_HOSTS` 为空时，主机将根据 ``['.localhost', '127.0.0.1', '[::1]']`` 进行验证, 并且允许使用 `localhost` 的子域。。

### 12、配置项 BASE_DIR 

`BASE_DIR` 指的是: 项目的顶级根目录,

新版的Django采用的是 `pathlib` 包中的 `Path` 模块来配置工程目录中的文件夹的：

```python
from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent
```

旧版的Django采用的是 `os` 包和 `sys` 包中的 `path` 模块来配置工程目录中的文件夹的

```python
import os
# Build paths inside the project like this: os.path.join(BASE_DIR, 'subdir') .
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
```

不管新版还是旧版， 上述方法中的 ``__file__`` 就是 ``settings.py`` 这个文件， 最终获取的 ``BASE_DIR`` 是: 项目的顶级根目录， 
``subdir`` 是顶级目录下的一个子目录(一般是个python包或普通文件夹)。

有时候，为了在开发过程中获得IDE的智能提示，我们把根目录和下面的一些子目录也加入到系统路径变量中， 如下所示：

```python
import sys
sys.path.insert(0, BASE_DIR) # 把根目录加入到系统路径变量中
sys.path.insert(0, os.path.join(BASE_DIR, 'subdir')) # 把根目录下的subdir加入到系统路径变量中
```

### 13、配置项 STATIC_URL、STATICFILES_DIRS 和 STATIC_ROOT

配置项 `STATIC_URL` 用于指定网站的静态文件(如 CSS, JavaScript, Images)的存储位置和访问路径。

下面给出开发过程中的 静态文件 的目录设置 和 路径配置 方法：

(1) 首先在工程根目录下 新建名为 `staticfiles` 的文件夹(该文件夹名字可以任意) ，我们可以把一些前端框架的CSS和JS文件都拷贝到里面，分门别类的组织起来。
目录结构如下所示：
```bash
mysite/
    mysite/
        __init__.py
        asgi.py
        settings.py
        urls.py
        wsgi.py
    staticfiles/
        bootstrap-5.1.3/
            bootstrap.bundle.js
            bootstrap.bundle.min.js
            bootstrap.css
            bootstrap.min.css
        font-awesome-4.7.0/
            font-awesome.css
            font-awesome.min.css
        jquery-3.6.0/
            jquery.js
            jquery.min.js
        images/
            bootstrap-logo.svg
            favicon.ico
    templates/
    manage.py
```

(2) URL 和 文件夹路径的引用配置方式有两种，
- 一种是使用 `pathlib` 包的配置方式：
```python
STATIC_URL = '/static/'
STATICFILES_DIRS = (BASE_DIR / 'staticfiles',) # 这是个元组，逗号不能丢
```
- 另一种是 `os` 包和 `sys` 包的配置方式：
```python
STATIC_URL = '/static/'
STATICFILES_DIRS = (os.path.join(BASE_DIR, 'staticfiles'),)  # 这是个元组，逗号不能丢
```

需要指出的是：用新版的`pathlib` 包来引用工程目录的时候，引入静态资源文件的时候，IDE不会自动提示，不太方便；

但是用旧版的 `os` 包和 `sys` 包来引用工程目录的时候，引入静态资源文件的时候，IDE会自动提示目录和文件，供你选择，非常方便。

在前端页面引入静态文件的方式，如下所示 ：

首先在HTML**页面顶部**加载模板标签命令: `{% load static %}` ，然后就可以用 `static` 命令引入静态文件了

```html
<link href="{% static 'bootstrap-5.1.3/bootstrap.css' %}" rel="stylesheet">
<link href="{% static 'font-awesome-4.7.0/font-awesome.css' %}" rel="stylesheet">
<script src="{% static 'jquery-3.6.0/jquery.js' %}"></script>
<script src="{% static 'bootstrap-5.1.3/bootstrap.bundle.js' %}"></script>
```

上述静态资源文件夹 `STATICFILES_DIRS` 和URL的配置用于开发过程。

在部署阶段，会使用 `django-admin collectstatic` 把所有App中的静态文件全部收集到一个单文件夹中，
此时，我们可以在根目录下新建一个新的文件夹，比如 名为 'statics'。 这个时候，我们需要配置 `STATIC_ROOT` 这个配置项，如下所示：

```python
STATIC_URL = '/static/'
STATICFILES_DIRS = (os.path.join(BASE_DIR, 'staticfiles'),)  # 用于开发阶段，可以有多个静态文件夹，所以是个元组
STATIC_ROOT = os.path.join(BASE_DIR, 'statics')  #用于部署阶段，这是单个文件夹，不是元组， 开发的时候可以暂不配置
```

执行命令 `django-admin collectstatic` 以后， ``STATICFILES_DIRS`` 中指定的所有内容，包括其他第三方App中的静态文件都会被全部拷贝到 
`STATIC_ROOT` 指定的文件夹中。 

### 14、配置项 MEDIA_URL 和 MEDIA_ROOT

这两个配置项 `MEDIA_URL` 和 `MEDIA_ROOT` 是为了指出: 用户上传的多媒体文件(Media Files，如文本/音频/视频文件等等)的存放目录和访问的url。

这两个配置项的配置方式也分为两种：

- 一种是使用 `pathlib` 包的配置方式：
```python
MEDIA_URL = '/media/'           # 这个url路径可以任意给定，一般就保持默认
MEDIA_ROOT = BASE_DIR / 'media' # 这个 根目录下的 文件夹的名字也可以任意，但一般也保持默认 
```
- 另一种是 `os` 包和 `sys` 包的配置方式：
```python
MEDIA_URL = '/media/'           # 这个url路径可以任意给定，一般就保持默认
MEDIA_ROOT = os.path.join(BASE_DIR, 'media') # 这个 根目录下的 文件夹的名字也可以任意，但一般也保持默认
```

我们还需要在根目录下新建一个文件夹 'media' 用于存放用户上传的各类文件，此时，目录结构如下所示：

```bash
mysite/
    media/
    mysite/
        __init__.py
        asgi.py
        settings.py
        urls.py
        wsgi.py
    staticfiles/
        bootstrap-5.1.3/
            bootstrap.bundle.js
            bootstrap.bundle.min.js
            bootstrap.css
            bootstrap.min.css
        font-awesome-4.7.0/
            font-awesome.css
            font-awesome.min.css
        jquery-3.6.0/
            jquery.js
            jquery.min.js
        images/
            bootstrap-logo.svg
            favicon.ico
    templates/
    manage.py
```

注意，在PyCharm 中， 上述文件夹的顺序是按照字母表的顺序排列的，顺序的上上下下并无关系，只要保证目录层级关系不变就可以。
所以，我对上面的目录顺序做了一些调整，纯粹是为了清晰和方便的展示目录层级关系。

准备好上述内容后，我们还需要指定 media 文件的访问服务方式：

- 在部署阶段一般是使用NGINX的静态资源代理来提供服务的；具体配置方式在后面讲到部署的时候会详说； 
- 在开发阶段，django为我们提供了一个专门的视图 (`django.views.static.serve`) 来为 media 文件提供访问服务。

具体配置方法是：在 全局路由配置文件(即 `ShangDaXue/urls.py`)中加入一个路由，并指定其视图，如下所示：

```python
from django.conf import settings
from django.contrib import admin
from django.urls import path, re_path
from django.views.static import serve

urlpatterns = [
    # 后台管理系统相关URL
    path('admin/', admin.site.urls),
    # 用于访问本地媒体文件的URL
    re_path(r'media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
]
```
由于使用了正则化表达式 `r'media/(?P<path>.*)$'` 来匹配url，所以用 `re_path(...)` 而不是 `path(...)`。

或者，像下面这样配置，把只在Debug模式下的路由放在一起：

```python
from django.conf import settings
from django.contrib import admin
from django.urls import path, re_path
from django.views.static import serve

urlpatterns = [
    path('admin/', admin.site.urls),
]

# DEBUG = true 模式下的路由配置
if settings.DEBUG:
    urlpatterns += [
        # 用于访问本地媒体文件的URL
        re_path(r'media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
    ]
```

我们注意到 ，在 ``contrib`` 包中还有一个视图(`django.contrib.staticfiles.views.serve`)也可以用于文件访问服务，
但是这个视图的内部最终还是调用了上面的视图(`django.views.static.serve`), 只是多做了一些检查工作。

如果想在模板中使用 `{{ MEDIA_URL }}`，在 `TEMPLATES` 的 `'context_processors'` 选项中添加 
`'django.template.context_processors.media'`。

其实， `django.views.static.serve` 这个视图也可以为 static 文件提供服务，后面我们会提到。

### 15、 配置项 TEMPLATES

一个包含所有 Django 模板引擎的配置的列表。列表中的每一项都是一个字典，包含了各个引擎的选项。

下面是一个配置，告诉 Django 模板引擎从每个安装好的应用程序中的 ``mysite/templates/`` 子目录中加载模板：

```python
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates']
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
```

上述配置字典中，每一项的解释如下：

(1) BACKEND¶
要使用的模板后端。内置的模板后端有：
```python
'django.template.backends.django.DjangoTemplates'  # django自己的模板系统
'django.template.backends.jinja2.Jinja2'     # 这个模板系统在Flask框架中也使用广泛
```
你可以通过将 BACKEND 设置为一个完全限定的路径（例如 'mypackage.whatever.Backend'）来使用一个不在 Django 中的模板后端。

(2) DIRS¶
按照搜索顺序，引擎应该查找模板源文件的目录。

下面这种写法是由于 `BASE_DIR` 是使用 'pathlib' 来指定的，
```python
'DIRS': [BASE_DIR / 'templates'] #
```
如果 `BASE_DIR` 是使用 'os.path' 来指定的，那么配置应该如下所示：
```python
'DIRS': [os.path.join(BASE_DIR, 'templates'), ]
```

(3) APP_DIRS¶
引擎是否应该在已安装的应用(App)中查找模板源文件。

存放模板的文件夹可以有多个，甚至每个App下面都可以放置一个 ``templates`` 文件夹，只存放与当前App相关的前端HTML模板。

默认的 `settings.py` 文件由 `django-admin startproject` 创建，设置 ``'APP_DIRS': True`` 。如果把它设为 `False`，则模板引擎就不会去
每个App的目录下查找模板源文件了。

(4) 'OPTIONS'¶
要传递给模板后台的额外参数。根据模板后端的不同，可用的参数也不同。
在上面的配置中，默认添加了 一些 模板上下文处理器 ，每个处理器的具体作用不在这里赘述。

### 10、配置项 DATABASES ：

一个包含所有数据库配置的字典，用于 Django。它是一个嵌套的字典，其内容是将一个数据库别名映射到一个包含单个数据库选项的字典中。

DATABASES 配置必须设置一个 `default` 数据库；也可以指定任何数量的其他数据库。

最简单的配置文件是使用 `SQLite` 的单数据库配置。可以通过以下方式进行配置：

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',  # 这个地方也可以使用：os.path.join(BASE_DIR, 'db.sqlite3')
    }
}
```
当连接到其他数据库后端时，如 MariaDB、MySQL、Oracle 或 PostgreSQL，将需要额外的连接参数。

请参阅下面的 ENGINE 配置，了解如何指定其他数据库类型。这个例子是针对 MySQL：

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'mydatabase',
        'USER': 'mydatabaseuser',
        'PASSWORD': 'mypassword',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}
```


## 六、创建自己的配置文件
配置文件的安全性至关重要。由于配置文件包含敏感信息, 比如数据库密码, 所以应该限制对配置文件的访问。
比如更改文件权限, 从而只能令你和你的WEB服务器能读取配置文件. 这在共享主机的环境中非常重要!

一个重要的工程实践是 把配置文件的项分到三种类型的文件中：

a、用于项目开发的配置文件: settings_develop.py

b、用于生产环境的配置文件: settings_product.py

c、基础配置文件(上述两个文件中的共有项目): settings_base.py

## 七、在独立运行的Python脚本中使用配置文件