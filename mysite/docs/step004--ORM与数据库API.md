# ORM 与 数据库API

现在让我们进入交互式 Python 命令行，尝试一下 Django 为你创建的各种 API。通过以下命令打开 Python 命令行：

```bash
python manage.py shell
```

我们使用这个命令而不是简单的使用“python”是因为 `manage.py` 会设置 `DJANGO_SETTINGS_MODULE` 环境变量，
这个变量会让 Django 根据 ``mysite/settings.py`` 文件来设置 Python 包的导入路径。

在pycharm 中运行上述命令，可以看到，打开了 Python 的 控制台，而且导入了一些包和路径，还调用了`django.setup()` 做了一些额外的准备工作:

```bash
import sys; print('Python %s on %s' % (sys.version, sys.platform))
import django; print('Django %s' % django.get_version())
sys.path.extend(['F:\\WebProjects\\django-tutorials\\mysite', 'D:\\Programs\\JetBrains\\PyCharm\\plugins\\python\\helpers\\pycharm', 'D:\\Programs\\JetBrains\\PyCharm\\plugins\\python\\helpers\\pydev'])
if 'setup' in dir(django): django.setup()
import django_manage_shell; django_manage_shell.run("F:/WebProjects/django-tutorials/mysite")
PyDev console: starting.
Python 3.9.1 (default, Dec 11 2020, 09:29:25) [MSC v.1916 64 bit (AMD64)] on win32
Django 3.2.9
>>>
```
当你成功进入命令行后，来试试 数据库 API 吧：

## 使用ORM-API 创建和查询Question数据

```bash
>>> from polls.models import Choice, Question  # 从polls包中导入我们设计的数据模型.

# 数据库中还没有Question对应的数据项
>>> Question.objects.all()
<QuerySet []>

# 创建一个新的 Question 对象
# 由于在之前的设置中关闭了django的时区自动感知，所以，这里 timezone.now()就等价于 datetime.datetime.now()
>>> from django.utils import timezone
>>> q = Question(question_text="What's new?", pub_date=timezone.now())

# 执行完上面的语句，Question 类的对象就创建好了，只不过此时只是在内存中的一个对象而已，
# 这个对象并没有进入数据库(可以查看 polls_question 这个数据表来验证)

# 要想把上面创建的对象保存到数据库中， 你必须主动显式的调用  save() 方法。
>>> q.save()
# 这个对象现在就进入数据库(可以查看 polls_question 这个数据表来验证)

# 数据对象进入数据库以后，才会给它分配一个id号.
>>> q.id
1

# 通过Python对象的属性来访问数据对象的字段.
>>> q.question_text
"What's new?"
>>> q.pub_date
datetime.datetime(2021, 11, 23, 10, 12, 49, 2576)

# 修改Python对象的属性(attributes), 然后调用 save() 方法就可以修改数据库中的字段内容.
>>> q.question_text = "What's up?"
>>> q.save()  # 不调用此方法，对属性的修改是不会主动保存到数据库的 

# objects.all() 展示了数据库的 `polls_question` 表中的所有数据
>>> Question.objects.all()
<QuerySet [<Question: Question object (1)>]>
```

可以查看 polls_question 这个数据表的内容如下：

![polls_question 这个数据表的内容](./images/step004/s4img001.png)

`<Question: Question object (1)>` 对于我们了解这个对象的细节没什么帮助。

让我们通过编辑 `Question` 模型的代码（位于 `polls/models.py` 中）来修复这个问题。

给 `Question` 和 `Choice` 增加 `__str__()` 方法(这其实是Python的魔法函数), 如下所示：

```python
from django.db import models

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.question_text

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text
```

给模型增加 `__str__()` 方法是很重要的，这不仅仅能给你在命令行里使用带来方便，Django 自动生成的 admin 里也使用这个方法来表示对象。

让我们再为 `Question` 模型添加一个自定义方法：

```python
import datetime

from django.db import models
from django.utils import timezone

class Question(models.Model):
    # ...
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
```

新加入的 `import datetime` 和 `from django.utils import timezone` 分别导入了 Python 的标准 `datetime` 模块和 
Django 中与时区相关的 `django.utils.timezone` 工具模块。

保存 `polls/models.py` 文件然后通过 `python manage.py shell` 命令再次打开 Python 交互式命令行：

```bash
>>> from polls.models import Choice, Question

# 确保刚才添加的 __str__() 可以工作.
>>> Question.objects.all()
<QuerySet [<Question: What's up?>]>
```

Django 提供了一整套丰富的数据库查询 API，它们完全是通过关键字参数(keyword arguments)来驱动的
```bash
>>> Question.objects.filter(id=1)
<QuerySet [<Question: What's up?>]>
>>> Question.objects.filter(question_text__startswith='What')
<QuerySet [<Question: What's up?>]>
>>> Question.objects.filter(question_text__endswith='up?')
<QuerySet [<Question: What's up?>]>
>>> Question.objects.filter(question_text__icontains="'s")
<QuerySet [<Question: What's up?>]>

# 获得今年发布的所有Question条目
>>> from django.utils import timezone
>>> current_year = timezone.now().year
>>> Question.objects.get(pub_date__year=current_year)
<Question: What's up?>

# 请求一个并不存在的数据ID, 这将会引发异常.
>>> Question.objects.get(id=2)
Traceback (most recent call last):
    ...
raise self.model.DoesNotExist(
polls.models.Question.DoesNotExist: Question matching query does not exist.

# 通过主键(primary key)来查询数据是最常见的使用情形，因此 Django 为主键精准查询提供了一个快捷方式(shortcut)
# 下面这条语句等同于  Question.objects.get(id=1). 
>>> Question.objects.get(pk=1)
<Question: What's up?>
# 需要注意的是 数据库中是没有pk这个字段的，它指向了实际上的字段(ID)

# 确保我们自定义的方法可以工作.
>>> q = Question.objects.get(pk=1)
>>> q.was_published_recently()
True
```

## 使用ORM-API 创建和查询Choice数据

```bash
# Give the Question a couple of Choices. The create call constructs a new
# Choice object, does the INSERT statement, adds the choice to the set
# of available choices and returns the new Choice object. Django creates
# a set to hold the "other side" of a ForeignKey relation
# (e.g. a question's choice) which can be accessed via the API.
>>> q = Question.objects.get(pk=1)

# Display any choices from the related object set -- none so far.
>>> q.choice_set.all()
<QuerySet []>

# Create three choices.
>>> q.choice_set.create(choice_text='Not much', votes=0)
<Choice: Not much>
>>> q.choice_set.create(choice_text='The sky', votes=0)
<Choice: The sky>
>>> c = q.choice_set.create(choice_text='Just hacking again', votes=0)

# Choice objects have API access to their related Question objects.
>>> c.question
<Question: What's up?>

# And vice versa: Question objects get access to Choice objects.
>>> q.choice_set.all()
<QuerySet [<Choice: Not much>, <Choice: The sky>, <Choice: Just hacking again>]>
>>> q.choice_set.count()
3

# The API automatically follows relationships as far as you need.
# Use double underscores to separate relationships.
# This works as many levels deep as you want; there's no limit.
# Find all Choices for any question whose pub_date is in this year
# (reusing the 'current_year' variable we created above).
>>> Choice.objects.filter(question__pub_date__year=current_year)
<QuerySet [<Choice: Not much>, <Choice: The sky>, <Choice: Just hacking again>]>

# Let's delete one of the choices. Use delete() for that.
>>> c = q.choice_set.filter(choice_text__startswith='Just hacking')
>>> c.delete()

```

## 在独立的Python脚本中使用ORM-API工具

我们可以从启动shell的时候pycharm的输出中获得灵感，

```bash
import sys; print('Python %s on %s' % (sys.version, sys.platform))
import django; print('Django %s' % django.get_version())
sys.path.extend(['F:\\WebProjects\\django-tutorials\\mysite', 'D:\\Programs\\JetBrains\\PyCharm\\plugins\\python\\helpers\\pycharm', 'D:\\Programs\\JetBrains\\PyCharm\\plugins\\python\\helpers\\pydev'])
if 'setup' in dir(django): django.setup()
import django_manage_shell; django_manage_shell.run("F:/WebProjects/django-tutorials/mysite")
PyDev console: starting.
Python 3.9.1 (default, Dec 11 2020, 09:29:25) [MSC v.1916 64 bit (AMD64)] on win32
Django 3.2.9
>>>
```

从上面可以看出，最关键的是下面这几句 ：

```python
import sys
print('Python %s on %s' % (sys.version, sys.platform))
import django
print('Django %s' % django.get_version())
if 'setup' in dir(django): 
    django.setup()
```

我们在 ``mysite`` 根目录下创建一个python包， 取名为 ``dbtools``， 专门用来存放可以独立运行的利用ORM机制对数据库进行操作的python脚本。

这里的 “**独立运行**” 指的是: 不用启动服务器，不用通过访问某个URL发起请求找到对应的视图，就可以直接访问数据库。

然后在 ``mysite/dbtools`` 内部新建一个py文件，假设取名为 ``polls_db.py``， 然后把上那几句关键代码拷贝进去，
直接运行这个脚本(请注意不是启动服务器)，发现报错了。 出错信息如下：

```bash
D:\Programs\Miniconda3\envs\djg329env\python.exe F:/WebProjects/django-tutorials/mysite/dbtools/polls_db.py
Python 3.9.1 (default, Dec 11 2020, 09:29:25) [MSC v.1916 64 bit (AMD64)] on win32
Django 3.2.9
Traceback (most recent call last):
  File "F:\WebProjects\django-tutorials\mysite\dbtools\polls_db.py", line 7, in <module>
    django.setup()
  File "D:\Programs\Miniconda3\envs\djg329env\lib\site-packages\django\__init__.py", line 19, in setup
    configure_logging(settings.LOGGING_CONFIG, settings.LOGGING)
  File "D:\Programs\Miniconda3\envs\djg329env\lib\site-packages\django\conf\__init__.py", line 82, in __getattr__
    self._setup(name)
  File "D:\Programs\Miniconda3\envs\djg329env\lib\site-packages\django\conf\__init__.py", line 63, in _setup
    raise ImproperlyConfigured(
django.core.exceptions.ImproperlyConfigured: Requested setting LOGGING_CONFIG, but settings are not configured. You must either define the environment variable DJANGO_SETTINGS_MODULE or call settings.configure() before accessing settings.

```

从上面的报错信息，可以看出，我们需要在 调用 ``django.setup()`` 之前先设置 环境变量 ``DJANGO_SETTINGS_MODULE``，那么该如何设置呢？
我们可以在 ``wsgi.py`` 这个文件中找到答案。

所以，现在 ``mysite/dbtools/polls_db.py`` 这个文件的内容如下所示：

```python
import sys
print('Python %s on %s' % (sys.version, sys.platform))
import django
print('Django %s' % django.get_version())
# 在调用django.setup()读取配置文件的时候，必须要设置 `DJANGO_SETTINGS_MODULE` 环境变量
# 把它设置为 当前项目的配置文件的点路径
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mysite.settings')

if 'setup' in dir(django):
    django.setup()
```

接下来直接运行上述脚本，发现不报错了，这就说明 使用 Django ORM 机制的启动环境已经设置好了

紧接着，我们就可以 导入 设计好的数据模型类 来访问数据库了。

完整的 ``mysite/dbtools/polls_db.py`` 内容如下所示：

```python
import sys
print('Python %s on %s' % (sys.version, sys.platform))
import django
print('Django %s' % django.get_version())


if __name__ == '__main__':
    # 在调用django.setup()读取配置文件的时候，必须要设置 `DJANGO_SETTINGS_MODULE` 环境变量
    # 把它设置为 当前项目的配置文件的点路径
    import os
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mysite.settings')

    # 设置django的启动环境(Configure the settings)
    if 'setup' in dir(django):
        django.setup()

    # 所有数据模型类的导入都必须放在 `django.setup()` 之后
    from polls.models import Choice, Question

    query_set = Question.objects.all()

    print(query_set)

    q = Question.objects.get(pk=1)
    print(q)

    qs = Question.objects.filter(pk=1)
    print(qs)

    choices = q.choice_set.all()
    print(choices)

    choices = Choice.objects.all()
    print(choices)
```

上面添加的三行注释需要特别注意。

运行上面的脚本文件，就可以得到如下结果：

```bash
D:\Programs\Miniconda3\envs\djg329env\python.exe F:/WebProjects/django-tutorials/mysite/dbtools/polls_db.py
Python 3.9.1 (default, Dec 11 2020, 09:29:25) [MSC v.1916 64 bit (AMD64)] on win32
Django 3.2.9
<QuerySet [<Question: What's up?>]>
What's up?
<QuerySet [<Question: What's up?>]>
<QuerySet [<Choice: Not much>, <Choice: The sky>]>
<QuerySet [<Choice: Not much>, <Choice: The sky>]>

```

## 使用脚本批量导入数据
在项目的实际开发过程中，往往需要准备大量的基础数据，用于调式，测试等。 

### 1、准备数据
首先准备大量的问卷数据，每一条数据都包括两个内容：一个是 问题(Question), 另一个是与之对应的 若干选项(Choice)，
我们在`mysite/dbtools/`目录下新建一个py文件 `polls_data.py`， 把这些数据用python的列表形式(list)组织起来，
每一个数据项以字典(dict)的形式呈现，文件内容如下所示：

```python
polls_data = [
    {
        'question': '您的性别',
        'choices': ['男', '女']
    },
    {
        'question': '您的年级',
        'choices': ['大一', '大二', '大三', '大四', ]
    },
    {
        'question': '您在校期间的平均月消费',
        'choices': ['600-1000', '1000-1500', '1500-2000', '2000以上']
    },
    {
        'question': '您的生活费来源',
        'choices': ['全部来自家庭', '部分来自家庭，部分靠自己赚取', '全部靠自己赚取']
    },
    {
        'question': '您每月用于校内食堂就餐的费用约为',
        'choices': ['300以下', '300-600', '600-1000', '1000以上']
    },
    {
        'question': '您每月用于零食及校外饮食的费用',
        'choices': ['无', '1-200', '200-500', '500以上']
    },
    {
        'question': '您购物的方式通常为',
        'choices': ['实体店', '网购', '兼而有之']
    },
    {
        'question': '您每月购置生活用品及衣物的费用',
        'choices': ['100以下', '100-500', '500-1000', '1000以上']
    },
    {
        'question': '每学期在化妆品或护肤品方面的花费',
        'choices': ['无', '200以内', '200-500', '500-1000', '1000以上']
    },
    {
        'question': '每学期用于购买学习资料、课外读物的费用',
        'choices': ['无', '100以内', '100-200', '200-500', '500以上']
    },
    {
        'question': '与同学或朋友间聚餐',
        'choices': ['从不', '一月一次或两次', '一周一次或两次', '200-500', '500以上']
    },
    {
        'question': '你是否会主动给父母购买衣物食品',
        'choices': ['是', '否']
    },
    {
        'question': '您觉得自己的生活费',
        'choices': ['有富余', '刚好够用', '不够']
    },
    {
        'question': '您的月消费多用在哪些方面',
        'choices': ['伙食', '购置衣物', '交通通讯', '生活用品', '日常交际', '学习用品', '娱乐旅游']
    },
]

# 以上数据来自 问卷星 https://www.wjx.cn/libt/10000.aspx
```
上面的数据中，模型的有些字段并没出现(如Question的`pub_date`字段 和 Choice的`votes`字段)，
在导入的时候，我们可以给其提供一些随机值或默认值。

### 2、写导入脚本

导入数据的脚本的基本运行逻辑就是：在遍历数据集的过程中，实例化一个Model的object，然后将这个object保存到数据库。

在 ``mysite/dbtools/polls_db.py`` 脚本文件中，加入下面的函数``def import_polls_data()``，再调用之：

```python
import random
import sys
print('Python %s on %s' % (sys.version, sys.platform))
import django
print('Django %s' % django.get_version())


def test_orm_api():
    # 所有数据模型类的导入都必须放在 `django.setup()` 之后
    from polls.models import Choice, Question

    query_set = Question.objects.all()

    print(query_set)

    q = Question.objects.get(pk=1)
    print(q)

    qs = Question.objects.filter(pk=1)
    print(qs)

    choices = q.choice_set.all()
    print(choices)

    choices = Choice.objects.all()
    print(choices)


def import_polls_data():
    # 所有数据模型类的导入都必须放在 `django.setup()` 之后
    from polls.models import Choice, Question
    # 为时间字段提供值
    from django.utils import timezone
    # 导入准备好的数据集
    from dbtools.polls_data import polls_data
    # 遍历数据集中的每一条数据，将其写入数据库
    for key, value in enumerate(polls_data):
        print(key, " --> ", value.get("question"))
        # 首先，实例化一个问题类的对象(Question object)
        qst = Question(question_text=value.get("question"), pub_date=timezone.now())
        # 接着，判断一下上述问题是否已经存在(导入过程中出现数据不规范，有可能会多次运行脚本文件)
        qset = Question.objects.filter(question_text=qst.question_text)
        # 接着， 如果此数据还不存在，将此对象写入数据库
        if qset is None:
            qst.save()
            # 接着，实例化该问题对应的所有选项(Choice objects)
            for ckey, cval in enumerate(value.get("choices")):
                print("===> ", ckey, " --> ", cval)
                # 实例化 Choice 的第一种方法: 先产生对象，再调用 save() 方法
                chc = Choice(choice_text=cval, votes=random.randint(10, 100), question=qst)
                chc.save()
                # # # 实例化 Choice 的第二种方法: 直接调用 create(...) 方法
                # qst.choice_set.create(choice_text=cval, votes=random.randint(10, 100))


if __name__ == '__main__':

    # 在调用django.setup()读取配置文件的时候，必须要设置 `DJANGO_SETTINGS_MODULE` 环境变量
    # 把它设置为 当前项目的配置文件的点路径
    import os
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mysite.settings')

    # 设置django的启动环境(Configure the settings)
    if 'setup' in dir(django):
        django.setup()

    # 测试ORM-API是否能够正常工作
    # test_orm_api()

    # 导入为 polls app 准备好的数据
    import_polls_data()
```

执行完上述脚本之后，我们可以查看数据表中的数据，如下所示：

- `polls_question`数据表中的数据

![polls_question数据表中的数据](./images/step004/s4img002.png)

- `polls_choice` 数据表中的数据

![polls_choice数据表中的数据](./images/step004/s4img003.png)