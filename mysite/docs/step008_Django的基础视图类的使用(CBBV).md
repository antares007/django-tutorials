# Django 的基础视图类(Class-based Basic View)

视图(View)是可调用的，能接受用户的请求并返回响应。视图远不只是个函数，Django提供了一些可用作视图的类的示例，允许你通过继承和复用构建自己的视图并且复用这些代码。

基于类的视图(Class-based View)提供另一种将视图实现为 Python对象 而不是 函数 的方法。它们不能替代基于函数的视图，但与基于函数的视图相比，它们是有某些不同和优势的。

- 与特定的 HTTP 方法（GET, POST, 等等）关联的代码组织能通过 单独的方法 替代 条件分支 来解决。

- 面向对象技术（比如 mixins 多重继承）可用于将代码分解为可重用组件。

## Django内置的基于类的视图总览

### 1、基础视图 (Basic View)

以下三个类提供了创建 Django 视图所需的大部分功能。你可以把它们看作是 父类的视图，它们可以自己使用，也可以从父类继承。

Django 内置的许多基于类的视图都是从其他基于类的视图或各种混入中继承过来的。因为这个继承链非常重要，所以祖先类被记录在 祖先（MRO） 节标题下。
MRO 是方法解析顺序的缩写。

![django.views.generic.base.View](./images/step008/s8img001.png)

### 2、通用视图  (General View)

通用视图 分为 通用显示视图 、 通用编辑视图、 通用日期视图，如下所示

![django.views.generic.base.View](./images/step008/s8img002.png)

## 基础视图 View类 的用法

![django.views.generic.base.View](./images/step008/s8img003.png)
![django.views.generic.base.View](./images/step008/s8img004.png)
![django.views.generic.base.View](./images/step008/s8img005.png)
![django.views.generic.base.View](./images/step008/s8img006.png)

## 使用基础视图类代替基于函数的视图

让我们将我们的投票应用转换成使用基础视图类，这样我们可以删除许多我们的代码。我们仅仅需要做以下几步来完成转换，我们将：

1、将基于函数的视图转换为基于基础视图类的视图。

2、使用 as_view 转换 URLconf。

下一步，我们将删除旧的 `index`, `detail`, 和 `results` 函数型视图，并用 Django 的基于类的视图代替。打开 `polls/views.py` 文件，并将它修改成：

```python
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views import View

from .models import Question, Choice


class QuestionIndexView(View):

    def get(self, request):
        latest_question_list = Question.objects.order_by('-pub_date')[:10]
        context = {
            'latest_question_list': latest_question_list
        }
        return render(request=request, template_name='polls/index.html', context=context)


class QuestionDetailView(View):

    def get(self, request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        return render(request, 'polls/detail.html', {'question': question})


class VoteResultView(View):

    def get(self, request, *args, **kwargs):
        question = get_object_or_404(Question, pk=kwargs.get('question_id'))
        return render(request, 'polls/results.html', {'question': question})


class VoteCountView(View):

    def post(self, request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        try:
            selected_choice = question.choice_set.get(pk=request.POST['choice'])
        except (KeyError, Choice.DoesNotExist):
            # 重新渲染表单
            return render(request, 'polls/detail.html', {
                'question': question,
                'error_message': "你没有选择任何一个选项!"
            })
        else:
            selected_choice.votes += 1
            selected_choice.save()
            # 成功的处理完POST数据之后，总是要返回一个 HttpResponseRedirect 对象
            # 这可以防止用户提交两次同样的表单数据，如果他点击了返回键
            return HttpResponseRedirect(redirect_to=reverse('polls:results', args=(question.id,)))

```

值得注意的是，你的方法返回值和基于函数的视图返回值是相同的，既某种形式的 `HttpResponse` 。这意味着 http 快捷函数 或 TemplateResponse 对象可以使用基于类里的视图。

## 修改 URLconf

因为 Django 的 URL 解析器期望发送请求和相关参数来 **调动函数而不是类** ，基于类的视图有一个 `as_view()` 类方法，
当一个请求到达的 URL 被关联模式匹配时，这个类方法返回一个函数。这个函数创建一个类的实例，调用 `setup()` 初始化它的属性，然后调用 `dispatch()` 方法。 
`dispatch` 观察请求并决定它是 **GET** 和 **POST**，等等。如果它被定义，那么依靠请求来匹配方法，否则会引发 **HttpResponseNotAllowed** 。

首先，打开 `polls/urls.py` 这个 `URLconf` 并将它修改成：

```python
from django.urls import path

from . import views

app_name = 'polls'

urlpatterns = [
    # ex: /polls/
    path('', views.QuestionIndexView.as_view(), name='index'),
    # ex: /polls/5/
    path('<int:question_id>/', views.QuestionDetailView.as_view(), name='detail'),
    # ex: /polls/5/results/
    path('<int:question_id>/results/', views.VoteResultView.as_view(), name='results'),
    # ex: /polls/5/vote/
    path('<int:question_id>/vote/', views.VoteCountView.as_view(), name='vote'),
]
```

此处需要特别注意的是： 路由参数 ``question_id`` 是如何传入到视图类的`get(...)`和`post(...)`方法中去的。

## 使用 模板视图类(TemplateView)定义视图

![模板视图类(TemplateView)](./images/step008/s8img007.png)

我们的 `QuestionIndexView` 这个类，还可以直接继承 `TemplateView` 类， 如下所示：

```python
from django.views.generic.base import TemplateView

class QuestionIndexView(TemplateView):

    template_name = 'polls/index.html'

    def get_context_data(self, **kwargs):
        context = super(QuestionIndexView, self).get_context_data(**kwargs)
        context['latest_question_list'] = Question.objects.order_by('-pub_date')[:10]
        return context
```

## 使用Mixins

Mixins 是一个多继承技术，其中可组合多个父类的行为和属性。

![LoginRequiredMixin](./images/step008/s8img008.png)

我们的投票应用(polls) 中的 投票表单提交的时候，一般要求要登陆之后才能提交，所以可以在 ``VoteCounterView`` 中混入 `LoginRequiredMixin`，
如下所示：

```python
from django.contrib.auth.mixins import LoginRequiredMixin

class VoteCountView(LoginRequiredMixin, View):
    login_url = '/accounts/login/'
    redirect_field_name = 'redirect_to'

    def post(self, request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        try:
            selected_choice = question.choice_set.get(pk=request.POST['choice'])
        except (KeyError, Choice.DoesNotExist):
            # 重新渲染表单
            return render(request, 'polls/detail.html', {
                'question': question,
                'error_message': "你没有选择任何一个选项!"
            })
        else:
            selected_choice.votes += 1
            selected_choice.save()
            # 成功的处理完POST数据之后，总是要返回一个 HttpResponseRedirect 对象
            # 这可以防止用户提交两次同样的表单数据，如果他点击了返回键
            return HttpResponseRedirect(redirect_to=reverse('polls:results', args=(question.id,)))
```

如果在未登录的状态下，提交投票表单，就会跳转到登录的URL请求上去，但是目前我们还没有实现登录功能，所以会出现 HTTP 404 错误。
如下所示：

![LoginRequiredMixin](./images/step008/s8img009.png)

这个时候，我们可以打开后台系统，登陆之后在回到投票界面进行投票。

注意，如果你没有指定参数 `login_url` ，你需要确认 `settings.LOGIN_URL`  和登录视图是正确关联的。

例如，使用默认方式，在 根URL 配置文件 ``mysite/urls.py``  里添加下面这行：

```python
from django.contrib.auth.views import LoginView

path('accounts/login/', LoginView.as_view()),
```

`LoginView` 这个类也使用了 **Mixin机制** 。

`settings.LOGIN_URL` 也接受视图方法名和 **named URL patterns** 。这样你可以在 `URLconf` 里自由地重新映射你的登录视图，而不需更新配置文件。

## 装饰基于类的视图

基于类的视图的扩展不仅限于使用 `mixins` ，你也可以使用装饰器。因为基于类的视图不是函数，
所以根据你是使用 `as_view()` 还是创建子类，装饰它们的工作方式会有不同。

### 在 URLconf 中装饰

可以通过装饰 `as_view()` 方法的结果来调整基于类的视图。最简单的方法是在你部署视图的 URLconf 中执行此操作：

```python
from django.contrib.auth.decorators import login_required, permission_required
from django.views.generic import TemplateView

urlpatterns = [
    path('about/', login_required(TemplateView.as_view(template_name="secret.html"))),
    path('vote/', permission_required('polls.can_vote')(VoteView.as_view())),
]
```

这个方式在每个基本实例上应用装饰器。如果你想装饰视图的每个实例，你需要采用不同方式。

### 装饰类

装饰基于类的视图的每个实例，你需要装饰类定义本身。为此，你可以将装饰器应用到类的 `dispatch()` 方法。

类上的方法与独立函数完全不同，因此你不能应用函数装饰器到方法上——你需要先将它转换为方法装饰器。
`method_decorator` 装饰器转换函数装饰器为防范装饰器，这样它就被用在实例方法上。举例：

```python
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

class QuestionIndexView(TemplateView):
    template_name = 'polls/index.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
```

或者，更简洁的说，你可以用装饰类来代替，并作为关键参数 `name` 传递要被装饰的方法名：

```python
@method_decorator(login_required, name='dispatch')
class QuestionIndexView(TemplateView):
    template_name = 'secret.html'
```

如果你在一些地方使用了常见的装饰器，你可以定义一个装饰器列表或元组，并使用它而不是多次调用 `method_decorator()` 。这两个类是等价的：

```python
decorators = [never_cache, login_required]

@method_decorator(decorators, name='dispatch')
class ProtectedView(TemplateView):
    template_name = 'secret.html'

@method_decorator(never_cache, name='dispatch')
@method_decorator(login_required, name='dispatch')
class ProtectedView(TemplateView):
    template_name = 'secret.html'
```

装饰器将按照它们传递给装饰器的顺序来处理请求。在这个例子里，`never_cache()` 将在 `login_required()` 之前处理请求。

在这个例子里，`ProtectedView` 的每一个实例将被登录保护。

## 使用基础视图类(View)处理表单

处理表单的基础视图类(View)视图如下所示：

```python
from django.http import HttpResponseRedirect
from django.shortcuts import render

from .forms import MyForm

def myview(request):
    
    if request.method == "GET":
        form = MyForm(initial={'key': 'value'})
        return render(request, 'form_template.html', {'form': form})
    
    if request.method == "POST":
        form = MyForm(request.POST)
        if form.is_valid():
            # <process form cleaned data>
            return HttpResponseRedirect('/success/')
        else:
            return render(request, 'form_template.html', {'form': form})
        
```

类似的基于类的视图可能看起来像这样：

```python
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import View

from .forms import MyForm

class MyFormView(View):
    form_class = MyForm
    initial = {'key': 'value'}
    template_name = 'form_template.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            # <process form cleaned data>
            return HttpResponseRedirect('/success/')

        return render(request, self.template_name, {'form': form})
```

这是一个很小的案例，但你可以看到你可以选择通过覆盖类的任何属性来自定义这个视图，比如 form_class 。


