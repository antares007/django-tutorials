# 创建一个投票应用(poll app)

现在你的开发环境——这个“项目” ——已经配置好了，你可以开始干活了。

通过这个教程，我们将带着你创建一个基本的投票应用程序(poll application)。

它将由两部分组成：

- 一个让人们查看和投票的公共站点(public site)。
  
- 一个让你能添加、修改和删除投票的管理站点(admin site)。

在 Django 中，每一个应用都是一个 Python 包，并且遵循着相同的约定。Django 自带一个工具，可以帮你生成应用的基础目录结构，这样你就能专心写代码，
而不是创建目录了。

    项目(project) VS 应用(application)
    
    项目和应用有什么区别？应用是一个专门做某件事的网络应用程序——比如博客系统，或者公共记录的数据库，或者小型的投票程序。
    项目则是一个网站使用的配置和应用的集合。项目可以包含很多个应用。应用可以被很多个项目使用。

你的应用可以存放在任何 Python 路径 中定义的路径。在这个教程中，我们将在你的 manage.py 同级目录下创建投票应用。
这样它就可以作为顶级模块导入，而不是 `mysite` 的子模块。

## 一、创建app的初始目录结构

请确定你现在处于 manage.py 所在的目录下，然后运行这行命令来创建一个应用(app)：

```bash
python manage.py startapp polls
```

或者使用 pycharm 工具来创建，如下所示：

```bash
manage.py@mysite > startapp polls
D:\Programs\JetBrains\PyCharm\bin\runnerw64.exe ...
Tracking file by folder pattern:  migrations

Following files were affected 
 F:\WebProjects\django-tutorials\mysite\polls\migrations\__init__.py
Process finished with exit code 0
```

这将会创建一个 polls 目录，它的目录结构大致如下：

![polls 目录](./images/step003/img001.png)

上面自动产生的`.py`文件还缺少一个路由配置文件 `urls.py` ，需要自己手动加上去。

每个.py文件的功能 如下所示：

```bash
    polls\
        migrations           # 记录当前app中的models往数据库迁移的操作历史
            __init__.py
        __init__.py          # 指明当前的polls文件夹是一个python的package
        admin.py             # 用于把models.py中的数据模型注册到后台管理系统
        apps.py              # 用于设置当前app的配置信息
        models.py            # 用于定义数据模型，利用ORM机制映射到数据库的数据表
        tests.py             # 用于存放对当前app的视图进行测试的脚本
        views.py             # 用于编写响应ULR请求对应的视图函数或视图类
        urls.py              # 自己手动添加的，用于存放与当前app有关的路由列表
```

这个目录结构包括了投票应用(polls app)的全部内容， 是根据Django自带的App模板产生的，APP模板如下所示：

```bash
django\
  conf\
    app_template\
      migrations\
        __init__.py-tpl
      admin.py-tpl
      apps.py-tpl
      models.py-tpl
      tests.py-tpl
      views.py-tpl
      urls.py-tpl   # 自己添加的内容，这样以后创建App的时候，就可以自动产生 路由配置文件
```


## 二、编写第一个视图(View)

打开 `polls/views.py`，把下面这些 Python 代码输入进去：

```python
from django.http import HttpResponse
def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")
```

这是 Django 中最简单的视图。如果想看见效果，我们需要将一个 URL 映射到它——这就是我们需要 `URLconf` 的原因了。

## 三、编写第一个路由(url route)

为了创建 `URLconf`，请在 polls 目录里新建一个 urls.py 文件。

在 polls/urls.py 中，输入如下代码：

```python
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
]
```

下一步是要在根 `URLconf` 文件中指定我们创建的 polls.urls 模块。在 mysite/urls.py 文件的 `urlpatterns` 列表里插入一个 `include()`， 
如下：

```python
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('polls/', include('polls.urls')),
    path('admin/', admin.site.urls),
]
```

函数 `include()` 允许引用其它 `URLconfs`。每当 Django 遇到 `include()` 时，它会截断与此项匹配的 URL 的部分，
并将剩余的字符串发送到 `URLconf` 以供进一步处理。

我们设计 `include()` 的理念是使其可以即插即用。因为投票应用有它自己的 `URLconf( polls/urls.py )`，
他们能够被放在 "/polls/" ， "/fun_polls/" ，"/content/polls/"，或者其他任何路径下，这个应用都能够正常工作。

    何时使用 include()
    
    当包括其它 URL 模式时你应该总是使用 `include()` ， `admin.site.urls` 是唯一例外。

你现在把 `index` 视图添加进了 `URLconf`。通过以下命令验证是否正常工作：

```bash
python manage.py runserver
```

用你的浏览器访问 ``http://localhost:8000/polls/`` ，你应该能够看见 "Hello, world. You're at the polls index." ，
这是你在 index 视图中定义的。 如果你在这里得到了一个错误页面，检查一下你是不是正访问着`http://localhost:8000/polls/` 
而不应该是 `http://localhost:8000/` 。

函数 `path()` 具有四个参数，两个必须参数：`route` 和 `view`，两个可选参数：`kwargs` 和 `name`。现在，是时候来研究这些参数的含义了。

**path() 参数： route**

route 是一个匹配 URL 的准则（类似正则表达式）。当 Django 响应一个请求时，它会从 `urlpatterns` 的第一项开始，
按顺序依次匹配列表中的项，直到找到匹配的项。

这些准则不会匹配 `GET` 和 `POST` 参数或域名。例如，`URLconf` 在处理请求 `https://www.example.com/myapp/` 时，它会尝试匹配 `myapp/` 。
处理请求 `https://www.example.com/myapp/?page=3` 时，也只会尝试匹配 `myapp/` 。

**path() 参数： view**

当 Django 找到了一个匹配的准则，就会调用这个特定的视图函数，并传入一个 `HttpRequest` 对象作为第一个参数，被“捕获”的参数以关键字参数的形式传入。
稍后，我们会给出一个例子。

**path() 参数： kwargs**

任意个关键字参数可以作为一个字典传递给目标视图函数。本教程中不会使用这一特性。

**path() 参数： name**

为你的 URL 取名能使你在 Django 的任意地方唯一地引用它，尤其是在模板中。这个有用的特性允许你只改一个文件就能全局地修改某个 URL 模式。

## 三、数据库配置与模型迁移(Database)

通常，配置文件默认使用 SQLite 作为默认数据库。如果你不熟悉数据库，或者只是想尝试下 Django，这是最简单的选择。Python 内置 SQLite，
所以你无需安装额外东西来使用它。

当你开始一个真正的项目时，你可能更倾向使用一个更具扩展性的数据库，例如 MySQL，避免中途切换数据库这个令人头疼的问题。

如果你想使用其他数据库，你需要安装合适的 database bindings ，然后改变设置文件中 `DATABASES 'default' 项目中的一些键值：

- ENGINE -- 可选值有 'django.db.backends.sqlite3'，'django.db.backends.postgresql'，'django.db.backends.mysql'，
  或 'django.db.backends.oracle'。其它 可用后端。
-- NAME -- 数据库的名称。如果你使用 SQLite，数据库将是你电脑上的一个文件，在这种情况下，NAME 应该是此文件完整的绝对路径，包括文件名。
  默认值 BASE_DIR / 'db.sqlite3' 将把数据库文件储存在项目的根目录。

如果你不使用 SQLite，则必须添加一些额外设置，比如 USER 、 PASSWORD 、 HOST 等等。

**SQLite 以外的其它数据库**

如果你使用了 SQLite 以外的数据库，请确认在使用前已经创建了数据库。你可以通过在你的数据库交互式命令行中使用 "CREATE DATABASE database_name;" 
命令来完成这件事。

另外，还要确保该数据库用户中提供 mysite/settings.py 具有 "create database" 权限。这使得自动创建的 test database 能被以后的教程使用。

如果你使用 SQLite，那么你不需要在使用前做任何事——数据库会在需要的时候自动创建，目前为了聚焦到框架本身的学习上，我们暂且使用 SQLite， 配置如下：

```bash
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3')  # BASE_DIR / 'db.sqlite3',
    },
    # 'mysql': {
    #     'ENGINE': 'django.db.backends.mysql',
    #     'NAME': 'mydatabase',
    #     'USER': 'mydatabaseuser',
    #     'PASSWORD': 'mypassword',
    #     'HOST': '127.0.0.1',
    #     'PORT': '3306',
    # }
}
```

编辑 mysite/settings.py 文件前，先设置 `TIME_ZONE` 为你自己时区。

此外，关注一下文件头部的 `INSTALLED_APPS` 设置项。这里包括了会在你项目中启用的所有 Django 应用。
应用能在多个项目中使用，你也可以打包并且发布应用，让别人使用它们。

通常， `INSTALLED_APPS` 默认包括了以下 Django 的自带应用：

- django.contrib.admin -- 管理员站点， 你很快就会使用它。
- django.contrib.auth -- 认证授权系统。
- django.contrib.contenttypes -- 内容类型框架。
- django.contrib.sessions -- 会话框架。
- django.contrib.messages -- 消息框架。
- django.contrib.staticfiles -- 管理静态文件的框架。

这些应用被默认启用是为了给常规项目提供方便。

默认开启的某些应用需要至少一个数据表，所以，在使用他们之前需要在数据库中创建一些表。请执行以下命令：

```bash
python manage.py migrate
```
执行上述命令的时候，系统输出如下：

```bash
manage.py@mysite > migrate
D:\Programs\JetBrains\PyCharm\bin\runnerw64.exe ....
Tracking file by folder pattern:  migrations
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, sessions
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying auth.0010_alter_group_name_max_length... OK
  Applying auth.0011_update_proxy_permissions... OK
  Applying auth.0012_alter_user_first_name_max_length... OK
  Applying sessions.0001_initial... OK

Process finished with exit code 0
```

这个 `migrate` 命令检查 `INSTALLED_APPS` 设置，为其中的每个应用创建需要的数据表，至于具体会创建什么，
这取决于你的 `mysite/settings.py` 设置文件和每个应用的数据库迁移文件（就是 ``migrations`` 文件夹中的py文件）。
这个命令所执行的每个迁移操作(如 `Applying admin.0001_initial... OK`)都会在终端中显示出来。

从上面的执行过程可以看出， 此次迁移操作，共写入了 ``admin``, ``auth``, ``contenttypes``, ``sessions``  这么多个App的相关models。

我们可以使用**pycharm 的数据库连接工具**连接到根目录下的 ``db.sqlite3`` 这个数据库，打开查看里面创建的数据表。
如下所示：
```bash
db.sqlite3
  main
    tables
        auth_group
        auth_group_permissions
        auth_permission
        auth_user
        auth_user_groups
        auth_user_user_permissions
        django_admin_log
        django_content_type
        django_migrations
        django_session
        sqlite_master
        sqlite_sequence
  Server Objects
    modules
    routines
    collations
```
上面总共有 12 个数据表，每个数据表的命名规则是： `appname_modelname`。 

我们可以查看 `auth_user` 这个数据表的字段，
如下所示：
```bash
auth_user:
    id 
    password
    last_login
    is_superuser
    username
    first_name
    last_name
    email
    is_staff
    is_active
    date_joined
```

就像之前说的，为了方便大多数项目，我们默认激活了一些应用，但并不是每个人都需要它们。如果你不需要某个或某些应用，你可以在运行 migrate 
前毫无顾虑地从 `INSTALLED_APPS` 里注释或者删除掉它们。 **`migrate` 命令只会为在 `INSTALLED_APPS` 里声明了的应用(app)进行数据库迁移**。


## 四、设计数据模型(Design Data Model)

在 Django 里写一个数据库驱动的 Web 应用的第一步是定义模型 - 也就是数据库结构设计和附加的其它元数据。

**设计哲学**

A model is the single, definitive source of information about your data. 
It contains the essential fields and behaviors of the data you're storing. 
Django follows the DRY Principle. The goal is to define your data model in one place and automatically derive things from it.

来介绍一下迁移 - 举个例子，不像 Ruby On Rails，Django 的迁移代码是由你的模型文件自动生成的，它本质上是个历史记录，
Django 可以用它来进行数据库的滚动更新，通过这种方式使其能够和当前的模型匹配。

在这个投票应用(polls app)中，需要创建两个模型：问题 **Question** 和选项 **Choice**。
- **Question** 模型包括问题描述和发布时间。
- **Choice** 模型有两个字段，选项描述和当前得票数。每个选项属于一个问题，一个问题可以对应多个选项。

这些概念可以通过一个 Python 类来描述。按照下面的例子来编辑 polls/models.py 文件：

```python
from django.db import models

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
```

每个模型被表示为 `django.db.models.Model` 类的子类。每个模型有许多类变量，它们都表示模型里的一个数据库字段。

每个字段都是 `Field` 类的实例 - 比如，字符字段被表示为 `CharField` ，日期时间字段被表示为 `DateTimeField` 。这将告诉 Django 每个字段要处理的数据类型。

每个 `Field` 类实例变量的名字（例如 `question_text` 或 `pub_date` ）也是字段名，所以最好使用对机器友好的格式。你将会在 Python 代码里使用它们，而数据库会将它们作为列名。

你可以使用可选的选项来为 `Field` 定义一个人类可读的名字。这个功能在很多 Django 内部组成部分中都被使用了，而且作为文档的一部分。如果某个字段没有提供此名称，Django 将会使用对机器友好的名称，也就是变量名。在上面的例子中，我们只为 `Question.pub_date` 定义了对人类友好的名字。对于模型内的其它字段，它们的机器友好名也会被作为人类友好名使用。

定义某些 `Field` 类实例需要参数。例如 `CharField` 需要一个 `max_length` 参数。这个参数的用处不止于用来定义数据库结构，也用于验证数据，我们稍后将会看到这方面的内容。

`Field` 也能够接收多个可选参数；在上面的例子中：我们将 `votes` 的 `default` 也就是默认值，设为0。

注意在最后，我们使用 `ForeignKey` 定义了一个关系。这将告诉 Django，每个 `Choice` 对象都关联到一个 `Question` 对象。 Django 支持所有常用的数据库关系：多对一、多对多和一对一。

## 五、安装应用并迁移模型(Migrate Data Model)

上面的一小段用于设计数据模型的代码给了 Django 很多信息，通过这些信息，Django 可以：

- 为这个应用创建数据库 `schema`（即生成 CREATE TABLE 语句）。
- 创建可以与 `Question` 和 `Choice` 对象进行交互的 Python 数据库 API。

但是首先得把 polls 应用安装到我们的项目里。

**设计哲学**：Django 应用是 **“可插拔”** 的。你可以在多个项目中使用同一个应用。除此之外，你还可以发布自己的应用，因为它们并不会被绑定到当前安装的 Django 上。

为了在我们的工程中安装这个应用，我们需要在配置项 `INSTALLED_APPS` 中添加设置。因为 `PollsConfig` 类写在文件 `polls/apps.py` 中，
所以它的点式路径是 `'polls.apps.PollsConfig'`。在文件 `mysite/settings.py` 中 `INSTALLED_APPS` 子项添加点式路径后，它看起来像这样：

```python
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'polls.apps.PollsConfig'  # 安装 polls app
]
```

现在你的 Django 项目会包含 polls 应用。

```bash
python manage.py makemigrations polls
```

运行上面的 `makemigrations polls` 命令之后，你将会看到类似于下面这样的输出：

```bash
manage.py@mysite > makemigrations polls
Tracking file by folder pattern:  migrations
Migrations for 'polls':
  polls\migrations\0001_initial.py
    - Create model Question
    - Create model Choice

Following files were affected 
 F:\WebProjects\django-tutorials\mysite\polls\migrations\0001_initial.py
Process finished with exit code 0
```

通过运行 `makemigrations` 命令，Django 会检测你对模型文件的修改（在这种情况下，你已经取得了新的），并且把修改的部分储存为一次 迁移。

迁移是 Django 对于模型定义（也就是你的数据库结构）的变化的储存形式 - 它们其实也只是一些你磁盘上的文件。如果你想的话，你可以阅读一下你模型的迁移数据，
它被储存在 `polls/migrations/0001_initial.py` 里。别担心，你不需要每次都阅读迁移文件，但是它们被设计成人类可读的形式，
这是为了便于你手动调整 Django 的修改方式。

运行 `makemigrations` 命令之后，`polls` 文件夹下多了一个文件 `polls/migrations/0001_initial.py` , 
你可以打开看看这个文件的内容，但不要去主动修改它。 

现在，再次运行 migrate 命令，在数据库里创建新定义的模型的数据表：

在运行上面的 `makemigrations polls` 命令 之前， 我们先把 `django_migrations` 这个数据表中的内容记下来：

![运行 `migrate` 命令之前](./images/step003/img002.png)

命令运行以后的输出，如下：

```bash
manage.py@mysite > migrate polls
Tracking file by folder pattern:  migrations
Operations to perform:
  Apply all migrations: polls
Running migrations:
  Applying polls.0001_initial... OK

Process finished with exit code 0
```

`django_migrations` 这个数据表中的内容记下来：

![`django_migrations` 操作以后](./images/step003/img003.png)

这个 migrate 命令选中所有还没有执行过的迁移（Django 通过在数据库中创建一个特殊的表 `django_migrations` 来跟踪执行过哪些迁移）并应用在数据库上 - 也就是将你对模型的更改同步到数据库结构上。

**迁移是非常强大的功能，它能让你在开发过程中持续的改变数据库结构而不需要重新删除和创建表** - 它专注于使数据库平滑升级而不会丢失数据。我们会在后面的教程中更加深入的学习这部分内容，
现在，你只需要记住，**改变模型需要这三步**：

- 编辑 models.py 文件，改变模型。
- 运行 python manage.py makemigrations 为模型的改变生成迁移文件。
- 运行 python manage.py migrate 来应用数据库迁移。

数据库迁移被分解成**生成**和**应用**两个命令是为了让你能够在代码控制系统上提交迁移数据并使其能在多个应用里使用；这不仅仅会让开发更加简单，也给别的开发者和生产环境中的使用带来方便。

