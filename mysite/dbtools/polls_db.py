import random
import sys
print('Python %s on %s' % (sys.version, sys.platform))
import django
print('Django %s' % django.get_version())


def test_orm_api():
    # 所有数据模型类的导入都必须放在 `django.setup()` 之后
    from polls.models import Choice, Question

    query_set = Question.objects.all()

    print(query_set)

    q = Question.objects.get(pk=1)
    print(q)

    qs = Question.objects.filter(pk=1)
    print(qs)

    choices = q.choice_set.all()
    print(choices)

    choices = Choice.objects.all()
    print(choices)


def import_polls_data():
    # 所有数据模型类的导入都必须放在 `django.setup()` 之后
    from polls.models import Choice, Question
    # 为时间字段提供值
    from django.utils import timezone
    # 导入准备好的数据集
    from dbtools.polls_data import polls_data
    # 遍历数据集中的每一条数据，将其写入数据库
    for key, value in enumerate(polls_data):
        print(key, " --> ", value.get("question"))
        # 首先，实例化一个问题类的对象(Question object)
        qst = Question(question_text=value.get("question"), pub_date=timezone.now())
        # 接着，判断一下上述问题是否已经存在(导入过程中出现数据不规范，有可能会多次运行脚本文件)
        qset = Question.objects.filter(question_text=qst.question_text)
        # 接着， 如果此数据还不存在，将此对象写入数据库
        if qset is None:
            qst.save()
            # 接着，实例化该问题对应的所有选项(Choice objects)
            for ckey, cval in enumerate(value.get("choices")):
                print("===> ", ckey, " --> ", cval)
                # 实例化 Choice 的第一种方法: 先产生对象，再调用 save() 方法
                chc = Choice(choice_text=cval, votes=random.randint(10, 100), question=qst)
                chc.save()
                # # # 实例化 Choice 的第二种方法: 直接调用 create(...) 方法
                # qst.choice_set.create(choice_text=cval, votes=random.randint(10, 100))


if __name__ == '__main__':

    # 在调用django.setup()读取配置文件的时候，必须要设置 `DJANGO_SETTINGS_MODULE` 环境变量
    # 把它设置为 当前项目的配置文件的点路径
    import os
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mysite.settings')

    # 设置django的启动环境(Configure the settings)
    if 'setup' in dir(django):
        django.setup()

    # 测试ORM-API是否能够正常工作
    # test_orm_api()

    # 导入为 polls app 准备好的数据
    import_polls_data()