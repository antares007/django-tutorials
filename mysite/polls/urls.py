from django.urls import path

from . import views

app_name = 'polls'

urlpatterns = [
    # ex: /polls/
    path('', views.QuestionIndexView.as_view(), name='index'),
    # ex: /polls/5/
    path('<int:question_id>/', views.QuestionDetailView.as_view(), name='detail'),
    # ex: /polls/5/results/
    path('<int:question_id>/results/', views.VoteResultView.as_view(), name='results'),
    # ex: /polls/5/vote/
    path('<int:question_id>/vote/', views.VoteCountView.as_view(), name='vote'),
]