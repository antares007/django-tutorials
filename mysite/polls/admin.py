from django.contrib import admin
from django.contrib.admin import ModelAdmin, AdminSite

from .models import Question, Choice

# 全局配置空字段或空字符串的显示效果为：'(None)'
AdminSite.empty_value_display = '(None)'


class QuestionAdmin(ModelAdmin):
    date_hierarchy = 'pub_date'
    list_display = ['id', 'question_text', 'pub_date']
    list_display_links = ['id', 'question_text',]  # None
    # list_editable = ['pub_date']
    list_filter = ['pub_date']
    # list_per_page = 8
    ordering = ['pub_date', ]
    search_fields = ['question_text']


class ChoiceAdmin(ModelAdmin):
    date_hierarchy = 'question__pub_date'
    list_display = ['id', 'choice_text', 'votes', 'question']
    list_display_links = ['id', 'choice_text']  # None
    list_editable = ['votes']
    # list_filter = ['votes']
    # list_per_page = 25
    autocomplete_fields = ['question']
    readonly_fields = ['question']


admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice, ChoiceAdmin)

