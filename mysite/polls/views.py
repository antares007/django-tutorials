from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import TemplateView

from .models import Question, Choice


class QuestionIndexView(View):

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(QuestionIndexView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        latest_question_list = Question.objects.order_by('-pub_date')[:10]
        context = {
            'latest_question_list': latest_question_list
        }
        return render(request=request, template_name='polls/index.html', context=context)

#
# class QuestionIndexView(TemplateView):
#
#     template_name = 'polls/index.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(QuestionIndexView, self).get_context_data(**kwargs)
#         context['latest_question_list'] = Question.objects.order_by('-pub_date')[:10]
#         return context


class QuestionDetailView(View):

    def get(self, request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        return render(request, 'polls/detail.html', {'question': question})


class VoteResultView(View):

    def get(self, request, *args, **kwargs):
        question = get_object_or_404(Question, pk=kwargs.get('question_id'))
        return render(request, 'polls/results.html', {'question': question})


class VoteCountView(LoginRequiredMixin, View):
    login_url = '/accounts/login/'
    redirect_field_name = 'redirect_to'

    def post(self, request, question_id):
        question = get_object_or_404(Question, pk=question_id)
        try:
            selected_choice = question.choice_set.get(pk=request.POST['choice'])
        except (KeyError, Choice.DoesNotExist):
            # 重新渲染表单
            return render(request, 'polls/detail.html', {
                'question': question,
                'error_message': "你没有选择任何一个选项!"
            })
        else:
            selected_choice.votes += 1
            selected_choice.save()
            # 成功的处理完POST数据之后，总是要返回一个 HttpResponseRedirect 对象
            # 这可以防止用户提交两次同样的表单数据，如果他点击了返回键
            return HttpResponseRedirect(redirect_to=reverse('polls:results', args=(question.id,)))
