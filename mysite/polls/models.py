import datetime

from django.db import models
from django.utils import timezone


class Question(models.Model):
    question_text = models.CharField(verbose_name="问题内容", max_length=200)
    pub_date = models.DateTimeField(verbose_name='发布时间')

    class Meta:
        verbose_name = "问题"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)


class Choice(models.Model):
    question = models.ForeignKey(Question, verbose_name='所属问题', on_delete=models.CASCADE)
    choice_text = models.CharField(verbose_name='选项内容', max_length=200)
    votes = models.IntegerField(verbose_name='投票数量', default=0)

    class Meta:
        verbose_name = "选项"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.choice_text

